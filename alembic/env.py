from __future__ import with_statement
from alembic import context
from sqlalchemy import create_engine
from logging.config import fileConfig
from membership.database import models # noqa: F401 ignore unused import needed for autogenerate

# add the project path to allow imports from membership
import inspect
import sys
from os.path import abspath, dirname

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
from config import DATABASE_URL
from membership.database.base import metadata

project_dir = dirname(dirname(abspath(inspect.getfile(inspect.currentframe()))))
print("Adding '{}' to PYTHONPATH".format(project_dir))
sys.path.append(project_dir)

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

# target_metadata = mymodel.Base.metadata
target_metadata = metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = DATABASE_URL
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        # see http://blog.code4hire.com/2017/06/setting-up-alembic-to-detect-the-column-length-change/
        compare_type=True,
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = create_engine(DATABASE_URL)
    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            # see http://blog.code4hire.com/2017/06/setting-up-alembic-to-detect-the-column-length-change/
            compare_type=True,
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
