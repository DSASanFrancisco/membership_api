#!/bin/sh
set -efx -o pipefail

##
# Build variables
##
if [ -z "$REGISTRY" ]; then
    echo "Missing environment variable REGISTRY from .gitlab.ci.yml"
    exit 1
fi
if [ -z "$TAG" ]; then
    echo "Missing environment variable TAG from .gitlab.ci.yml"
    exit 1
fi
if [ -z "$NAMESPACE" ]; then
    export NAMESPACE="dsasanfrancisco"
fi

# Copy the .env file
if [ -f ".env" ]; then
    # Must be testing locally
    echo "Found existing .env file. Are you running this locally?"
    echo "Please backup your .env file first and then run this script"
    exit 1
fi
cp ci/ci.env .env

NORMALIZED_TAG=$(printf '%s' "$TAG" | tr '/' '-')
export NORMALIZED_TAG

# Run the migrations to confirm that they work and prep the database for tests
docker-compose -f docker-compose.yml -f ci/docker-ci.yml up migrate
