from __future__ import absolute_import

import logging
from os import path

try:
    from dotenv import load_dotenv
    dotenv_path = path.normpath(path.join(path.dirname(__file__), '..', '.env'))
    logging.info("Loading environment variable map from dotenv file ({})...".format(dotenv_path))
    if path.exists(dotenv_path):
        load_dotenv(dotenv_path)
        logging.info("Finished loading os.environ from dotenv file")
    else:
        logging.warning("Couldn't find dotenv file at: {}".format(dotenv_path))
except ImportError:
    logging.debug("Package 'dotenv' not installed (preferred for production), skipping .env import")
    pass
