from typing import Optional

from config.lib import from_env, throw

USE_EMAIL: bool = from_env.get_bool('USE_EMAIL', False)
EMAIL_CONNECTOR: str = from_env.get_str('EMAIL_CONNECTOR', 'mailgun')
EMAIL_DOMAIN: str = from_env.get_str('EMAIL_DOMAIN', 'dsasf.org')
EMAIL_API_ID: Optional[str] = from_env.get_str(
    'EMAIL_API_ID',
    throw if USE_EMAIL and EMAIL_CONNECTOR == 'mailjet' else None)
EMAIL_API_KEY: Optional[str] = from_env.get_str('EMAIL_API_KEY', throw if USE_EMAIL else None)
MAILGUN_URL: str = from_env.get_str('MAILGUN_URL', 'https://api.mailgun.net/v3/routes')

SUPPORT_EMAIL_ADDRESS: str = from_env.get_str(
    'SUPPORT_EMAIL_ADDRESS',
    f'tech-support@{EMAIL_DOMAIN}',
)

DISABLE_WELCOME_EMAIL: bool = from_env.get_bool('DISABLE_WELCOME_EMAIL', False)

SMTP_HOST: Optional[str] = from_env.get_str(
    'SMTP_HOST',
    throw if USE_EMAIL and EMAIL_CONNECTOR == 'smtp' else None)
SMTP_PORT: Optional[int] = from_env.get_int(
    'SMTP_PORT',
    throw if USE_EMAIL and EMAIL_CONNECTOR == 'smtp' else None)
SMTP_USERNAME: Optional[str] = from_env.get_str('SMTP_USERNAME', None)
SMTP_PASSWORD: Optional[str] = from_env.get_str('SMTP_PASSWORD', None)
