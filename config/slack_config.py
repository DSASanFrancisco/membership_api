from typing import Optional

from slack_sdk import WebClient

from config import from_env, throw

SLACK_ENABLED: Optional[bool] = from_env.get_bool('SLACK_ENABLED', False)
SLACK_API_TOKEN: Optional[str] = from_env.get_str(
    'SLACK_API_TOKEN', throw if SLACK_ENABLED else None)
SLACK_TEAM_ID: Optional[str] = from_env.get_str('SLACK_TEAM_ID', throw if SLACK_ENABLED else None)
SLACK_ONBOARDING_CHANNEL_ID: Optional[str] = from_env.get_str(
    'SLACK_ONBOARDING_CHANNEL_ID', throw if SLACK_ENABLED else None)
SLACK_INTRO_CHANNEL_ID: Optional[str] = from_env.get_str(
    'SLACK_INTRO_CHANNEL', throw if SLACK_ENABLED else None)

slack_client = WebClient(token=SLACK_API_TOKEN)
