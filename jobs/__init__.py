import argparse
import sys
import importlib
import pkgutil
from typing import IO, Any, AnyStr, Callable, Generic, List, Optional, TypeVar

from overrides import overrides


class Job:
    cmd: str
    description: Optional[str]

    def __init__(self):
        # by default the name of the job is the module name to where it is located
        if not getattr(self, 'name', None):
            self.cmd = self.__module__.split('.', maxsplit=1)[1]

    def build_parser(self, parser: argparse.ArgumentParser) -> None:
        pass

    def run(self, config: dict) -> None:
        pass


def list_all() -> List[Job]:
    for _, name, _ in pkgutil.iter_modules([__name__]):
        importlib.import_module('.' + name, __name__)
    return [j() for j in Job.__subclasses__()]


V = TypeVar('V')


class ConfirmAction(Generic[V]):
    def __init__(self, value: V, description: str):
        self.value = value
        self.description = description

    def __enter__(self) -> Optional[V]:
        if self.value:
            print(self.description)
            resp = input("Continue? (Y/n) ")
            return self.value if not resp or resp.lower() == 'y' else None
        else:
            return self.value

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class LogAndContinue(Callable[[Any, str], ConfirmAction[V]]):
    def __init__(self, out: IO[AnyStr] = sys.stdout):
        self._out = out

    def __call__(self, value: Any, description: str):
        self._out.write(description + '\n')
        return ForceConfirmAction(value, description)


class ForceConfirmAction(ConfirmAction):
    @overrides
    def __enter__(self) -> Optional[V]:
        return self.value
