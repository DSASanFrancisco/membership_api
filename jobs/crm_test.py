from datetime import datetime

from dateutil.relativedelta import relativedelta
from overrides import overrides

from jobs import Job
from membership.database.base import Session
from membership.database.models import Member
from membership.services import PhoneNumberService
from membership.services.crm.crm_service import CrmService


class CrmTest(Job):
    imported = set(
        [])

    @overrides
    def run(self, config: dict) -> None:
        service = CrmService(PhoneNumberService())

        session = Session()

        two_months_ago = datetime.now() - relativedelta(months=2)

        members = session.query(Member). \
            filter(Member.date_created >= two_months_ago) \
            .all()

        imported_members = [m for m in members if m.email_address in self.imported]
        not_imported_members = [m for m in members if m.email_address not in self.imported]

        results = service.bulk_sync_members(
            imported_members, "National Import")
        print(results.to_json())

        results = service.bulk_sync_members(
            not_imported_members, "unknown (website or national import)")
        print(results.to_json())
