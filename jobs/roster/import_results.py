from typing import Dict, List, Optional, Set  # NOQA: F401

import jobs.roster.ambiguous_link as link
from membership.database.models import Member  # NOQA: F401
from membership.database.models import Identity  # NOQA: F401
from membership.database.models import PhoneNumber  # NOQA: F401
from membership.database.models import NationalMembershipData  # NOQA: F401
from membership.database.models import Role  # NOQA: F401
from membership.schemas import JsonObj


class ImportResults:
    def __init__(self):
        self.members_created: Set[int] = set()
        self.members_updated: Set[int] = set()
        self.members_onboarded: Set[int] = set()
        self.memberships_created: Set[int] = set()
        self.memberships_updated: Set[int] = set()
        self.identities_added: Set[int] = set()
        self.phone_numbers_added: Set[int] = set()
        self.phone_numbers_failed: Set[str] = set()
        self.ambiguous_links: List['link.AmbiguousLink'] = list()
        self.errors: List[Exception] = list()
        self.processed: int = 0

    def __str__(self):
        return str(self.to_json())

    def to_json(self, member_cache: Optional[Dict[int, Member]] = None) -> JsonObj:
        return {
            "total_processed": self.processed,
            "total_failed_records": len(self.errors),
            "total_upgraded_guests": len(self.members_onboarded),
            "national_records_created": len(self.memberships_created),
            "national_records_updated": len(self.memberships_updated),
            "chapter_records_created": len(self.members_created),
            "chapter_records_updated": len(self.members_updated),
            "new_members": [
                self.member_to_json(member_cache.get(member_id))
                for member_id in self.members_onboarded
            ],
            "ambiguous_links": [
                self.link_to_json(link, member_cache)
                for link in self.ambiguous_links
            ],
            "errors": [e.to_json() for e in self.errors]
        }

    def link_to_json(self, link: 'link.AmbiguousLink', member_cache: Dict[int, Member]) -> JsonObj:
        return {
            'csv_record': link.csv_record.to_json(),
            'matching_chapter_records': [
                self.member_to_json(member_cache.get(mem_id))
                for mem_id in link.matching_chapter_records
            ]
        }

    def member_to_json(self, m: Member) -> JsonObj:
        dues_paid_overridden = (
            m.dues_paid_overridden_on.strftime("%Y-%m-%d")
            if m.dues_paid_overridden_on
            else None
        )

        return {
            'id': m.id,
            'date_created': m.date_created.strftime("%Y-%m-%d") if m.date_created else None,
            'first_name': m.first_name,
            'last_name': m.last_name,
            'email_address': m.email_address,
            'normalized_email': m.normalized_email,
            'do_not_call': m.do_not_call,
            'do_not_email': m.do_not_email,
            'dues_paid_overridden_on': dues_paid_overridden,
            'onboarded_on': m.onboarded_on.strftime("%Y-%m-%d") if m.onboarded_on else None,
            'suspended_on': m.suspended_on.strftime("%Y-%m-%d") if m.suspended_on else None,
            'left_chapter_on': m.left_chapter_on.strftime("%Y-%m-%d") if m.left_chapter_on else None
        }
