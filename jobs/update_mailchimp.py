from typing import List

from requests import Response
from overrides import overrides

from config.mailchimp_config import ELIGIBILITY_LIST_ID
from jobs import Job
from membership.database.base import Session
from membership.database.models import Member
from membership.services.eligibility import MembershipAuditService, Json, MemberStatusUtil
from membership.services.mailchimp.mailchimp_action import MailchimpAction, UpdateTagsAction, \
    ActionResult
from membership.services.mailchimp.mailchimp_action_generator import MailchimpActionGenerator
from membership.services.mailchimp.mailchimp_service import MailchimpService


class MembershipAudit(Job):
    description = "Uploads all members to mailchimp and tags them based on voting eligibility."

    @overrides
    def run(self, config: dict) -> None:
        try:
            session = Session()

            members = {
                m.id: m
                for m in session.query(Member).all()
            }

            print("Building audit report...")
            membership_audit_service = MembershipAuditService()
            audit = membership_audit_service.build_membership_audit(session)

            eligibility_list = [
                (members[audit_item.member_id], audit_item)
                for audit_item in audit.items
            ]

            print("Generating mailchimp actions...")
            dues_cutoff = MemberStatusUtil.calc_dues_cutoff()
            generator = MailchimpActionGenerator()
            actions = generator.generate_eligibility_list_actions(eligibility_list, dues_cutoff)

            action_audit = self.build_action_audit(actions)
            print(str(action_audit))
            print()

            session.close()

            print("Uploading records to mailchimp")
            mailchimp_service = MailchimpService()
            responses = mailchimp_service.apply_mailchimp_actions(ELIGIBILITY_LIST_ID, actions)

            results_report = self.build_results_report(responses)
            print(str(results_report))
        except Exception as e:
            print("Something went wrong:")
            print(e)

            raise e

    def build_action_audit(self, actions: List[MailchimpAction]) -> Json:
        return {
            'eligible': self.tag_count('Eligible', actions),
            'eligible_leaving': self.tag_count('Eligible - Leaving Standing', actions),
            'ineligible_inactive': self.tag_count('Ineligible - Inactive', actions),
            'ineligible_standing': self.tag_count('Ineligible - Out of Standing', actions),
            'ineligible_both': self.tag_count('Ineligible - Both', actions),
            'ineligible_suspended': self.tag_count('Ineligible - Suspended', actions)
        }

    def tag_count(self, tag_label: str, actions: List[MailchimpAction]) -> int:
        tag_actions = [
            action.payload['tags']
            for action in actions
            if isinstance(action, UpdateTagsAction)
        ]

        matches = [
            tag
            for action_tags in tag_actions for tag in action_tags
            if tag['status'] == 'active' and tag['name'] == tag_label
        ]

        return len(matches)

    def build_results_report(self, action_results: List[ActionResult]) -> Json:
        return [
            self.error_response(result)
            for result in action_results
            if not result.is_success()
        ]

    def error_response(self, action_result: ActionResult) -> Json:
        if action_result.is_api_error() and isinstance(action_result.result, Response):
            return {
                'email': action_result.action.email,
                'type': 'API',
                'error': action_result.result.json()
            }
        else:
            return {
                'email': action_result.action.email,
                'type': 'SCRIPT',
                'error': str(action_result.result)
            }
