from datetime import datetime
from typing import Dict, List, Union


Json = Union[str, float, int, List['Json'], Dict[str, 'Json']]
JsonObj = Dict[str, Json]
JsonList = List[Json]


def format_datetime(dt: datetime) -> int:
    """
    Formats a datetime as milliseconds from the epoch for JavaScript
    """
    return round(dt.timestamp() * 1000)
