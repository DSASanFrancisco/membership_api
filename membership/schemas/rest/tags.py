from membership.database.models import Tag
from membership.schemas import JsonObj


def format_tag(tag: Tag) -> JsonObj:
    return {
        'id': tag.id,
        'name': tag.name,
        'date_added': tag.date_added
    }
