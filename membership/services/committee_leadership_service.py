"""Operations related to committees."""

from datetime import datetime
from typing import Optional

from dateutil.relativedelta import relativedelta

from membership.database.models import Committee, CommitteeLeader, CommitteeLeadershipRole, \
    LeadershipRole
from membership.models import AuthContext
from membership.schemas import JsonObj, JsonList
from membership.schemas.rest.committees import format_committee_leadership_role
from membership.services import ValidationError
from membership.services.errors import NotFoundError
from membership.util import time


class CommitteeLeadershipService:
    """Operations related to committee leadership."""
    class ErrorCodes:
        INVALID_TERM_END_DATE = "INVALID_TERM_END_DATE"

    def get_leadership_roles(self, ctx: AuthContext, committee_id: int) -> Optional[JsonList]:
        committee = ctx.session.query(Committee).get(committee_id)
        if committee is None:
            raise NotFoundError(f'Committee with id {committee_id} not found')
        return [format_committee_leadership_role(r) for r in committee.committee_leadership_roles]

    def add_leadership_role(
        self,
        ctx: AuthContext,
        committee_id: int,
        title: str,
        term_limit_months: Optional[int] = None,
    ) -> Optional[JsonObj]:
        ctx.az.verify_admin(committee_id)
        leadership_role = (
            ctx.session.query(LeadershipRole)
            .filter(LeadershipRole.title == title).one_or_none()
        )
        if leadership_role is None:
            leadership_role = LeadershipRole(title=title)
            ctx.session.add(leadership_role)
        committee_leadership_role = CommitteeLeadershipRole(
            committee_id=committee_id,
            term_limit_months=term_limit_months,
        )
        committee_leadership_role.leadership_role = leadership_role
        ctx.session.add(committee_leadership_role)
        ctx.session.commit()
        return format_committee_leadership_role(committee_leadership_role)

    def replace_leader(
        self,
        ctx: AuthContext,
        committee_leadership_role_id: int,
        member_id: Optional[int],
        term_start_date: Optional[datetime] = None,
        term_end_date: Optional[datetime] = None,
    ) -> Optional[JsonObj]:
        # TODO: should we validate that each member can hold at most one role per committee?
        committee_leadership_role = (
            ctx.session.query(CommitteeLeadershipRole)
            .get(committee_leadership_role_id)
        )
        if committee_leadership_role is None:
            raise NotFoundError(
                f'Committee leadership role with id {committee_leadership_role_id} not found'
            )
        ctx.az.verify_admin(committee_leadership_role.committee_id)
        if term_start_date is None:
            term_start_date = time.get_current_time()
        if committee_leadership_role.current_leader is not None:
            committee_leadership_role.current_leader.term_end_date = term_start_date
        if member_id:
            if committee_leadership_role.term_limit_months:
                expected_end_date = (
                    term_start_date
                    + relativedelta(months=committee_leadership_role.term_limit_months)
                )
                if term_end_date is None:
                    term_end_date = expected_end_date
                elif term_end_date > expected_end_date:
                    raise ValidationError(
                        CommitteeLeadershipService.ErrorCodes.INVALID_TERM_END_DATE,
                        (
                            f'Term end date {term_end_date.isoformat()}'
                            f' is beyond the term limit {expected_end_date.isoformat()}'
                        )
                    )
            if term_end_date is not None and term_start_date >= term_end_date:
                raise ValidationError(
                    CommitteeLeadershipService.ErrorCodes.INVALID_TERM_END_DATE,
                    (
                        f'Term end date {term_end_date.isoformat()}'
                        f' is before the term start date {term_start_date.isoformat()}'
                    )
                )
            new_leader = CommitteeLeader(
                committee_leadership_role_id=committee_leadership_role_id,
                member_id=member_id,
                term_start_date=term_start_date,
                term_end_date=term_end_date,
            )
            ctx.session.add(new_leader)
        ctx.session.commit()
        return format_committee_leadership_role(committee_leadership_role)
