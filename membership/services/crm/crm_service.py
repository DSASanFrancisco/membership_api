import logging
import time
from typing import List, Dict, Optional

from config import airtable
from membership.database.models import Member
from membership.schemas import JsonObj
from membership.services.crm.crm_record import CrmRecord
from membership.services.crm.crm_sync_result import CrmSyncResult, CrmSyncStatus
from membership.services.phone_number_service import PhoneNumberService

logger = logging.getLogger(__name__)


class SyncActions:

    def __init__(self, inserts: List[CrmRecord], updates: List[CrmRecord]):
        self.inserts = inserts
        self.updates = updates


# CRM = Comrade Relational Manager
# This is an Airtable Base used to track new members for onboarding outreach by
# the SF CCC = Chapter Coordination Committee
class CrmService:

    def __init__(self, number_service: PhoneNumberService):
        self.number_service = number_service

    def bulk_sync_members(self,
                          members: List[Member],
                          onboarding_source: str) -> (int, int):
        insert_count = 0
        update_count = 0

        try:
            # Airtable has low rate limits, so instead of searching records one at a time, we
            # bulk fetch the full sheet and set up a email -> record cache.
            found_records_cache: Dict[str, CrmRecord] = self._get_all_records()

            # Create a list of action that, when executed, will update the crm table to match
            # the current state of the member's information.
            sync_actions = self._map_members_to_sync_actions(
                members, onboarding_source, found_records_cache)

            # Airtable supports bulk inserts and bulk updates. Run inserts first because it's more
            # important to get the latest sign ups and updates could fail.
            if sync_actions.inserts:
                self._batch_inserts(sync_actions.inserts)
                insert_count = len(sync_actions.inserts)
            if sync_actions.updates:
                self._batch_update(sync_actions.updates)
                update_count = len(sync_actions.updates)

            return CrmSyncResult(
                email="bulk action",
                status=CrmSyncStatus.SUCCESS.name,
                insert_count=insert_count,
                update_count=update_count,
                message=f"Inserted {insert_count} record(s). Updated {update_count} record(s).")
        except Exception as e:
            return CrmSyncResult(
                email="bulk action",
                status=CrmSyncStatus.ERROR.name,
                insert_count=insert_count,
                update_count=update_count,
                message=str(e))

    def sync_member(self, member: Member, onboarding_source: str) -> CrmSyncResult:
        try:
            # Search the member by email address.
            matches = self._search(member.email_address)

            # If they are not already in the CRM, insert their record.
            if not matches:
                new_record = self._map_record(None, member, onboarding_source)
                self._insert_one(new_record)
                return CrmSyncResult(
                    email=member.email_address,
                    status=CrmSyncStatus.SUCCESS.name,
                    insert_count=1,
                    update_count=0,
                    message="Added member record!")

            # Otherwise, update any matching records. Note, there should only be one matching
            # record; however, it's possible for dup entries (due to human error). Instead of
            # failing, we'll update all matching records.
            updates = []
            for match in matches:
                # Until we start tracking onboarding_source in the db, we should just leave the
                # source that was originally written to the crm, otherwise, we may override it
                # incorrectly. TODO: I'll replace this in an upcoming PR -Kyle
                source = match.onboarding_source if match.onboarding_source else onboarding_source
                new_record = self._map_record(match.record_id, member, source)

                # Only update if the member record has changed.
                if new_record != match:
                    updates.append(new_record)

            self._batch_update(updates)

            return CrmSyncResult(
                email=member.email_address,
                status=CrmSyncStatus.SUCCESS.name,
                insert_count=0,
                update_count=len(updates),
                message=f"Updated {len(updates)} record(s).")
        except Exception as e:
            return CrmSyncResult(
                email=member.email_address,
                status=CrmSyncStatus.ERROR.name,
                insert_count=0,
                update_count=0,
                message=str(e))

    def _get_all_records(self) -> Dict[str, CrmRecord]:
        found_records = {}

        for page in self._get_paged_records():
            for raw_record in page:
                crm_record = CrmRecord.from_raw_record(raw_record)
                found_records[crm_record.email.lower()] = crm_record

        return found_records

    def _get_paged_records(self) -> List[JsonObj]:
        return airtable.get_iter()

    def _search(self, email: str) -> List[CrmRecord]:
        records = airtable.search("Email", email)
        return [CrmRecord.from_raw_record(record) for record in records]

    def _batch_update(self, updates: List[CrmRecord]) -> None:
        """ Internal Function to limit batch calls to API limit """
        responses = []
        for update in updates:
            responses.append(airtable.update(update.record_id, update.to_json()))
            time.sleep(airtable.API_LIMIT)
        return responses

    def _batch_inserts(self, inserts: CrmRecord) -> None:
        as_json = [insert.to_json() for insert in inserts]
        airtable.batch_insert(as_json)

    def _insert_one(self, record: CrmRecord) -> None:
        airtable.insert(record.to_json())

    def _map_members_to_sync_actions(self,
                                     members: List[Member],
                                     onboarding_source: str,
                                     crm_record_cache: Dict[str, CrmRecord]) -> SyncActions:
        updates = []
        inserts = []
        for member in members:
            match = crm_record_cache.get(member.email_address.lower(), None)

            if match:
                # Until we start tracking onboarding_source in the db, we should just leave the
                # source that was originally written to the crm, otherwise, we may override it
                # incorrectly. TODO: I'll replace this in an upcoming PR -Kyle
                source = match.onboarding_source if match.onboarding_source else onboarding_source
                new_record = self._map_record(match.record_id, member, source)

                # Only update if the member record has changed.
                if new_record != match:
                    updates.append(new_record)
            elif not match:
                new_record = self._map_record(None, member, onboarding_source)
                inserts.append(new_record)

        return SyncActions(inserts, updates)

    def _map_record(self,
                    record_id: Optional[str],
                    member: Member,
                    onboarding_source: str) -> CrmRecord:
        best_number = self.number_service.best_phone_for_calling(member.phone_numbers)
        join_date = member.date_created.strftime("%m/%d/%Y") if member.date_created else None

        return CrmRecord(
            record_id=record_id,
            email=member.email_address,
            first_name=member.first_name,
            last_name=member.last_name,
            phone=best_number.number if best_number else None,
            onboarding_source=onboarding_source,
            join_date=join_date,
            do_not_email=member.do_not_email,
            do_not_call=member.do_not_call
        )
