from datetime import datetime
from typing import List, Optional, Union

from membership.database.models import Attendee, Meeting, Member, NationalMembershipData
from membership.models import AttendeeAsEligibleToVote, MemberAsEligibleToVote
from membership.models.membership_status import MembershipStatus
from membership.repos import MeetingRepo
from membership.services import AttendeeService
from membership.services.eligibility.eligibility_service import \
    EligibilityService
from membership.services.errors import ValidationError
from membership.services.member_status_util import MemberStatusUtil
from membership.util.time import get_current_time
from sqlalchemy import orm


class SanFranciscoEligibilityService(EligibilityService):

    class ErrorCodes:
        MEETING_ENDED = "MEETING_ENDED"

    def __init__(self, meetings: MeetingRepo, attendee_service: AttendeeService) -> None:
        self.meetings = meetings
        self.attendee_service = attendee_service

    def members_as_eligible_to_vote(
        self,
        session: orm.Session,
        members: List[Member],
        since: Optional[datetime] = None
    ) -> List[MemberAsEligibleToVote]:
        if since is None:
            since = get_current_time()

        recent_meetings = self.meetings.most_recent(session, 3, since)

        return [self._eligibility_details(m, recent_meetings) for m in members]

    def update_eligibility_to_vote_at_attendance(
        self,
        session: orm.Session,
        member: Optional[Union[Member, int, str]] = None,
        meeting: Optional[Union[Meeting, int, str]] = None,
        attendee: Optional[Union[Attendee, int, str]] = None
    ) -> None:
        attendee: Attendee = self.attendee_service.retrieve_attendee(
            session,
            member,
            meeting,
            attendee
        )

        member: Member = attendee.member
        meeting: Meeting = attendee.meeting

        if meeting.end_time != None and meeting.end_time < get_current_time():
            raise ValidationError(
                SanFranciscoEligibilityService.ErrorCodes.MEETING_ENDED,
                'May not update eligibility to vote after meeting has ended.'
            )

        members_as_eligible_to_vote = self.members_as_eligible_to_vote(
            session, [member], meeting.start_time
        )

        if members_as_eligible_to_vote and members_as_eligible_to_vote[0].is_eligible:
            attendee.eligible_to_vote = True
        else:
            attendee.eligible_to_vote = False

        session.add(attendee)
        session.commit()

    @staticmethod
    def attendees_as_eligible_to_vote(meeting: Meeting) -> List[AttendeeAsEligibleToVote]:
        def attendee_as_eligible(attendee: Attendee) -> AttendeeAsEligibleToVote:
            return AttendeeAsEligibleToVote(
                attendee,
                message="{}eligible".format('' if bool(attendee.eligible_to_vote) else 'not '),
                dues_are_paid=None,
                meets_attendance_criteria=None,
                active_in_committee=None
            )

        return [attendee_as_eligible(a) for a in meeting.attendees]

    def _eligibility_details(self,
                             member: Member,
                             recent_meetings: List[Meeting]) -> MemberAsEligibleToVote:
        membership: Optional['NationalMembershipData'] = max(
            member.memberships_usa, key=lambda m: m.dues_paid_until, default=None)

        member_status = MemberStatusUtil.get_member_status(member, membership)
        status_is_eligible = (
            member_status != MembershipStatus.SUSPENDED
            and member_status != MembershipStatus.LEFT_CHAPTER
        )

        dues_cutoff = MemberStatusUtil.calc_dues_cutoff()
        dues_are_paid = (
            (membership is not None and membership.dues_paid_until >= dues_cutoff)
            or member.dues_paid_overridden_on is not None
        )

        """
        True if the member attended the last two out of three meetings or has an active role,
        False otherwise
        """
        attended_meeting_ids = {a.meeting_id for a in member.meetings_attended}
        recent_meeting_attendance = [
            m for m in recent_meetings if m.id in attended_meeting_ids
        ]
        has_attended = len(recent_meeting_attendance) >= 2
        active_in_committee = any(
            role.role == 'active' and role.committee is not None and not role.committee.provisional
            for role in member.roles
        )

        is_active = has_attended or active_in_committee
        is_eligible = dues_are_paid and is_active and status_is_eligible

        message = 'eligible' if is_eligible else 'not eligible'
        if recent_meeting_attendance:
            message += ' ({})'.format(
                ', '.join(m.start_time.strftime('%b') for m in recent_meeting_attendance)
            )
        return MemberAsEligibleToVote(
            member,
            is_eligible=is_eligible,
            message=message,
            dues_are_paid=dues_are_paid,
            meets_attendance_criteria=has_attended,
            active_in_committee=active_in_committee
        )

    def does_member_status_signify_dues_are_paid(self, member_status: MembershipStatus) -> bool:
        return (
            member_status == MembershipStatus.GOOD_STANDING
            or member_status == MembershipStatus.LEAVING_STANDING
        )
