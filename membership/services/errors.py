from collections import OrderedDict

from typing import Dict, Sequence, List, Union


class ValidationError(Exception):
    def __init__(self, key: str, message: str):
        super().__init__(message)
        self.key = key
        self.message = message


class NotFoundError(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class JsonFormatError(Exception):
    def __init__(self, path: Sequence[str], message: str):
        super().__init__(f"JsonFormatError: {message} @ {'.'.join(path)}")
        self.path = path
        self.message = message

    def __repr__(self):
        return f"JsonFormatError({repr(self.path)}, {repr(self.message)})"


class JsonValidationError(Exception):
    def __init__(self, errors: Union[List[JsonFormatError], Dict[str, str]]):
        if isinstance(errors, list):
            unsorted: Dict[str, JsonFormatError] = {
                '.'.join(e.path): e for e in errors
            }
        elif isinstance(errors, dict):
            unsorted: Dict[str, JsonFormatError] = {
                k: JsonFormatError(k, v) for k, v in errors.items()
            }
        else:
            raise TypeError(
                "expected 'errors' argument to be either List[JsonFormatError] "
                "or Dict[str, str] (ie. {'path.to.field': 'error message'})"
            )
        self.errors = OrderedDict(sorted(unsorted.items(), key=lambda e: e[0]))
        super().__init__(
            'JsonValidationError({'
            + ','.join(f"'{'.'.join(e.path)}': '{e.message}'" for k, e in self.errors.items())
            + '})'
        )

    def __repr__(self):
        error_dict = self.errors
        return f"JsonValidationError({sorted(error_dict)})"
