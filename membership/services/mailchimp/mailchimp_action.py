from typing import Dict, Union

from requests import Response

from membership.schemas import JsonObj, Json


class MailchimpAction:
    def __init__(self, email: str, payload: JsonObj):
        self.email = email
        self.payload = payload


class UpdateTagsAction(MailchimpAction):
    def __init__(self, email: str, payload: JsonObj):
        super().__init__(email, payload)

    def __str__(self) -> str:
        return str({
            'email': self.email,
            'type': 'UPDATE_TAGS',
            'payload': self.payload
        })


class AddOrUpdateRecordAction(MailchimpAction):
    def __init__(self,
                 email: str,
                 payload: Dict[str, object]):
        super().__init__(email, payload)

    def __str__(self) -> str:
        return str({
            'email': self.email,
            'type': 'ADD_OR_UPDATE',
            'payload': self.payload,
            'tag': self.tag_action.payload
        })


class ActionResult:
    def __init__(self, action: MailchimpAction, result: Union[Response, Exception]):
        self.action = action
        self.result = result

    def is_success(self) -> bool:
        is_script_error = self.is_script_error()
        has_success_code = (
            not is_script_error
            and (self.result.status_code == 200 or self.result.status_code == 204)
        )

        return has_success_code

    def is_script_error(self) -> bool:
        return not isinstance(self.result, Response)

    def is_api_error(self) -> bool:
        return not self.is_script_error() and not self.is_success()

    def error_message(self) -> Json:
        if self.is_script_error():
            return str(self.result)
        else:
            return self.result.text
