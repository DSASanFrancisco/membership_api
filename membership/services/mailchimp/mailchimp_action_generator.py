from datetime import datetime
from typing import List, Tuple

from dateutil.relativedelta import relativedelta

from membership.database.models import Member, MembershipStatusAuditItemization
from membership.models.membership_status import MembershipStatus
from membership.services.eligibility import MembershipAuditService
from membership.services.mailchimp.mailchimp_action import AddOrUpdateRecordAction, \
    UpdateTagsAction, MailchimpAction


class MailchimpActionGenerator:

    def generate_eligibility_list_actions(
        self,
        eligibility_lists: List[Tuple[Member, MembershipStatusAuditItemization]],
        dues_cutoff: datetime
    ) -> List[MailchimpAction]:
        actions = []

        i = 0
        for individual in eligibility_lists:
            (member, audit_item) = individual

            not_member = audit_item.membership_status == MembershipStatus.NOT_A_MEMBER.name
            if not_member or member.email_address is None:
                i += 1
                continue

            add_or_update_action = self.map_add_or_update_member_action(member)
            actions.append(add_or_update_action)

            tag_action = self.map_tags_action(member, audit_item, dues_cutoff)
            actions.append(tag_action)

        return actions

    def map_add_or_update_member_action(self, member: Member) -> MailchimpAction:
        unsubscribe = (member.do_not_email or member.suspended_on or member.left_chapter_on)

        payload = {
            'email_address': member.email_address,
            'status_if_new': 'unsubscribed' if unsubscribe else 'subscribed',
            'merge_fields': {
                'FNAME': member.first_name,
                'LNAME': member.last_name
            }
        }

        return AddOrUpdateRecordAction(member.email_address, payload)

    def map_tags_action(self,
                        member: Member,
                        audit_item: MembershipStatusAuditItemization,
                        dues_cutoff: datetime):
        email = member.email_address

        eligible = (
            audit_item.eligible_to_vote and (
                audit_item.membership_status == MembershipStatus.GOOD_STANDING.name
                or audit_item.membership_status == MembershipStatus.UNKNOWN.name
            )
        )

        eligible_leaving = (
            audit_item.eligible_to_vote
            and audit_item.membership_status == MembershipStatus.LEAVING_STANDING.name
        )

        ineligible_inactive = MembershipAuditService.ineligible_inactive(audit_item)
        ineligible_standing = MembershipAuditService.ineligible_standing(audit_item)

        ineligible_both = (
            MembershipAuditService.ineligible_both(audit_item)
            and audit_item.membership_status != MembershipStatus.SUSPENDED.name
        )

        ineligible_suspended = audit_item.membership_status == MembershipStatus.SUSPENDED.name

        one_month_before_cutoff = dues_cutoff - relativedelta(months=1)
        membership = max(member.memberships_usa, key=lambda m: m.dues_paid_until, default=None)
        just_left_grace_period = (
            audit_item.membership_status == MembershipStatus.OUT_OF_STANDING.name
            and one_month_before_cutoff < membership.dues_paid_until < dues_cutoff
        )

        payload = {
            'tags': [
                {
                    'name': 'Eligible',
                    'status': 'active' if eligible else 'inactive'
                },
                {
                    'name': 'Eligible - Leaving Standing',
                    'status': 'active' if eligible_leaving else 'inactive'
                },
                {
                    'name': 'Ineligible - Inactive',
                    'status': 'active' if ineligible_inactive else 'inactive'
                },
                {
                    'name': 'Ineligible - Out of Standing',
                    'status': 'active' if ineligible_standing else 'inactive'
                },
                {
                    'name': 'Ineligible - Both',
                    'status': 'active' if ineligible_both else 'inactive'
                },
                {
                    'name': 'Ineligible - Suspended',
                    'status': 'active' if ineligible_suspended else 'inactive'
                },
                {
                    'name': 'Just Left Grace Period',
                    'status': 'active' if just_left_grace_period else 'inactive'
                }
            ]
        }

        tag_count = len(list(filter(lambda x: x['status'] == 'active', payload['tags'])))
        if tag_count == 0:
            print("%s: %s, none" % (member.id, member.email_address))
        elif tag_count > 1:
            print("%s: %s, multiple" % (member.id, member.email_address))

        return UpdateTagsAction(email, payload)
