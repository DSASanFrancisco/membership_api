from typing import List, TypeVar

from membership.schemas import Json, JsonObj

MT = TypeVar('MailchimpTag')
MR = TypeVar('MailchimpRecord')


class MailchimpTag:
    def __init__(self, id, name: str):
        self.id = id
        self.name = name

    def from_json(json: Json) -> MT:
        return MailchimpTag(json['id'], json['name'])

    def to_json(self) -> JsonObj:
        return {
            'id': self.id,
            'name': self.name
        }


class MailchimpMember:
    def __init__(self,
                 rid,
                 email_address: str,
                 first_name: str,
                 last_name: str,
                 status,
                 tags: List[MailchimpTag]):
        self.rid = rid
        self.email_address = email_address
        self.first_name = first_name
        self.last_name = last_name
        self.status = status
        self.tags = tags

    def has_tag(self, tag_name: str) -> bool:
        return any(filter(lambda t: t.name == tag_name, self.tags))

    def from_json(json: JsonObj) -> MR:
        tags = [MailchimpTag.from_json(t) for t in json['tags']]
        return MailchimpMember(
            json['id'],
            json['email_address'],
            json['merge_fields']['FNAME'],
            json['merge_fields']['LNAME'],
            json['status'],
            tags)

    def to_json(self) -> JsonObj:
        return {
            'id': self.rid,
            'email_address': self.email_address,
            'merge_fields': {
                'FNAME': self.first_name,
                'LNAME': self.last_name
            },
            'status': self.status,
            'tags': [t.to_json() for t in self.tags]
        }

    def __str__(self) -> str:
        return str(self.to_json())
