from typing import Optional

from membership.schemas import JsonObj
from membership.services.onboarding.onboarding_status import OnboardingStatus


class OnboardingResult:

    def __init__(self, status: OnboardingStatus, message: Optional[JsonObj] = None):
        self.status = status
        self.message = message

    def to_json(self) -> JsonObj:
        return {
            'status': self.status.name,
            'message': self.message
        }


class OnboardingResults:

    def __init__(self,
                 auth0_result: OnboardingResult,
                 portal_email_result: OnboardingResult,
                 google_group_result: OnboardingResult,
                 slack_result: OnboardingResult,
                 newsletter_result: OnboardingResult,
                 hustle_result: OnboardingResult,
                 crm_result: OnboardingResult):
        self.auth0_result = auth0_result
        self.portal_email_result = portal_email_result
        self.google_group_result = google_group_result
        self.slack_result = slack_result
        self.newsletter_result = newsletter_result
        self.hustle_result = hustle_result
        self.crm_result = crm_result

    def to_json(self) -> JsonObj:
        return {
            'auth0_result': self.auth0_result.to_json(),
            'portal_email_result': self.portal_email_result.to_json(),
            'google_group_result': self.google_group_result.to_json(),
            'slack_result': self.slack_result.to_json(),
            'newsletter_result': self.newsletter_result.to_json(),
            'hustle_result': self.hustle_result.to_json(),
            'crm_result': self.crm_result.to_json()
        }
