import logging
from typing import Tuple

from config import USE_EMAIL, DISABLE_WELCOME_EMAIL, EMAIL_CONNECTOR, CRM_ENABLED, USE_AUTH
from config.mailchimp_config import NEWSLETTER_LIST_ID, MAILCHIMP_ENABLED
from config.slack_config import SLACK_ENABLED
from membership.database.models import Member
from membership.services.crm.crm_service import CrmService
from membership.services.mailchimp.mailchimp_action_generator import MailchimpActionGenerator
from membership.services.mailchimp.mailchimp_service import MailchimpService
from membership.services.onboarding.onboarding_results import OnboardingResult, OnboardingResults
from membership.services.onboarding.onboarding_status import OnboardingStatus
from membership.services.slack.slack_service import SlackService
from membership.util.email import send_welcome_email, get_email_connector
from membership.web.auth import create_auth0_user, CreateAuth0UserError

logger = logging.getLogger(__name__)


class OnboardingService:

    def __init__(self,
                 mailchimp_service: MailchimpService,
                 mailchimp_action_generator: MailchimpActionGenerator,
                 crm_service: CrmService,
                 slack_service: SlackService):
        self.mailchimp_service = mailchimp_service
        self.mailchimp_action_generator = mailchimp_action_generator
        self.crm_service = crm_service
        self.email_connector = get_email_connector(EMAIL_CONNECTOR)
        self.slack_service = slack_service

    def onboard_member(self,
                       member: Member,
                       onboarding_source: str,
                       create_portal_account: bool) -> OnboardingResults:
        # Adding to Google Groups would require us to upgrade to Google workspace.
        if create_portal_account:
            google_group_result = OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED)
        else:
            google_group_result = OnboardingResult(OnboardingStatus.SKIPPED)

        # Adding to Slack would require us to upgrade to Enterprise slack.
        slack_result = OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED)

        # Hustle API is in closed beta. We've reached out about joining it.
        hustle_result = OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED)

        # Ensure email address is string type. Otherwise, linter doesn't like it
        email = member.email_address if member.email_address is not None else ""
        (auth0_result, verify_url) = self._try_creating_auth0_account(email, create_portal_account)
        portal_email_result = self._try_sending_portal_email(
            member, verify_url, create_portal_account)
        newsletter_result = self._try_adding_to_newsletter(member)
        crm_result = self._try_syncing_to_crm(member, onboarding_source)

        onboarding_results = OnboardingResults(
            auth0_result=auth0_result,
            portal_email_result=portal_email_result,
            google_group_result=google_group_result,
            slack_result=slack_result,
            newsletter_result=newsletter_result,
            hustle_result=hustle_result,
            crm_result=crm_result
        )

        self._try_notifying_slack_onboarding_channel(member, onboarding_source, onboarding_results)

        return onboarding_results

    def _try_creating_auth0_account(self,
                                    member_email: str,
                                    create_portal_account: bool) -> Tuple[OnboardingResult, str]:
        if not create_portal_account:
            return OnboardingResult(OnboardingStatus.SKIPPED), None

        if not USE_AUTH:
            return OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED), None

        try:
            verify_url = create_auth0_user(member_email)
            auth0_result = OnboardingResult(OnboardingStatus.SUCCESS)
            return auth0_result, verify_url
        except CreateAuth0UserError as e:
            if e.status_code == 409:
                return OnboardingResult(OnboardingStatus.ALREADY_ADDED), None
            else:
                return OnboardingResult(OnboardingStatus.ERROR, str(e)), None

    def _try_sending_portal_email(self,
                                  member: Member,
                                  verify_url: str,
                                  create_portal_account: bool) -> OnboardingResult:
        if not create_portal_account:
            return OnboardingResult(OnboardingStatus.SKIPPED)

        # This should only apply to local/dev environments
        if not USE_EMAIL or DISABLE_WELCOME_EMAIL:
            return OnboardingResult(
                OnboardingStatus.NOT_IMPLEMENTED,
                "Emails have been disabled for the site.")

        if not verify_url:
            return OnboardingResult(
                OnboardingStatus.ERROR,
                "Verification url wasn't generated. Can't construct email.")

        try:
            send_welcome_email(self.email_connector, member, verify_url)
            return OnboardingResult(OnboardingStatus.SUCCESS)
        except Exception as e:
            return OnboardingResult(OnboardingStatus.ERROR, str(e))

    def _try_adding_to_newsletter(self, member: Member) -> OnboardingResult:
        if not MAILCHIMP_ENABLED:
            return OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED)

        if member.do_not_email:
            return OnboardingResult(OnboardingStatus.OPTED_OUT, "Member marked 'do not email'")

        action = self.mailchimp_action_generator.map_add_or_update_member_action(member)
        mailchimp_result = self.mailchimp_service.update_member(NEWSLETTER_LIST_ID, action)

        if mailchimp_result.is_success():
            return OnboardingResult(OnboardingStatus.SUCCESS)
        else:
            return OnboardingResult(OnboardingStatus.ERROR, mailchimp_result.error_message())

    def _try_syncing_to_crm(self, member: Member, onboarding_source: str) -> OnboardingResult:
        if not CRM_ENABLED:
            return OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED)

        if member.do_not_email and member.do_not_call:
            return OnboardingResult(
                OnboardingStatus.OPTED_OUT, "Member marked 'do not email' and 'do not call'")

        sync_result = self.crm_service.sync_member(member, onboarding_source)

        if sync_result.is_success():
            return OnboardingResult(OnboardingStatus.SUCCESS)
        else:
            return OnboardingResult(OnboardingStatus.ERROR, sync_result.message)

    def _try_notifying_slack_onboarding_channel(self,
                                                member: Member,
                                                onboarding_source: str,
                                                results: OnboardingResults) -> None:
        if SLACK_ENABLED:
            try:
                self.slack_service.notify_of_individual_sign_up(member, onboarding_source, results)
            except Exception as e:
                logger.error("Slack notification failed to send when onboarding new member "
                             f"with id: {member.id}: {str(e)}")
