import logging
from datetime import datetime
from logging import Logger
from typing import List, Optional

from membership.database.models import Role
from sqlalchemy import orm
from sqlalchemy.exc import IntegrityError


class RoleService:
    def __init__(self, logger: Logger = logging.getLogger(__name__)):
        self.logger = logger

    def add(
        self,
        session: orm.Session,
        member_id: int,
        role: str,
        committee_id: Optional[int],
        starting_at: datetime = datetime.now(),
        commit: bool = True,
    ) -> Optional[int]:
        """
        Adds the given role, while avoiding duplicates.

        :return: the id if a role is added, otherwise None
        """
        role = Role(
            member_id=member_id,
            role=role,
            committee_id=committee_id,
            date_created=starting_at,
        )
        existing_role: Optional[Role] = session.query(Role).filter_by(
            member_id=role.member_id, committee_id=role.committee_id, role=role.role,
        ).first()
        if existing_role is None:
            try:
                session.add(role)
                if commit:
                    session.commit()
                else:
                    session.flush()
                return role.id
            except IntegrityError as e:
                self.logger.debug(
                    f"Role already exists for member_id={member_id}, committee_id="
                    f"{committee_id}, role={role}",
                    exc_info=e,
                )
                session.rollback()

    def find_by_member_id(self, session: orm.Session, member_id: int) -> List[Role]:
        return session.query(Role).filter(Role.member_id == member_id).all()

    def remove(
        self,
        session: orm.Session,
        member_id: int,
        role: str,
        committee_id: Optional[int],
        remove_all: bool = False,
        commit: bool = True,
    ) -> bool:
        """
        Removes the first role that matches the given identifiers in natural order.

        :param remove_all: whether to remove all roles that match this description,
                           or just the first one. Typically, duplicate roles should
                           be prevented, but sometimes you want to make sure to delete
                           all instances.
        :param commit: whether to commit the changes to the session or just flush
        :return: True if a role was found and deleted, otherwise False
        """
        query = session.query(Role).filter_by(
            member_id=member_id, role=role, committee_id=committee_id,
        )
        if remove_all is True:
            existing_roles = query.all()
        else:
            existing_roles = [r for r in [query.first()] if r is not None]
        for r in existing_roles:
            session.delete(r)
            if commit:
                session.commit()
            else:
                session.flush()
        return len(existing_roles) > 0
