import json
import logging
import requests

from overrides import overrides
from typing import Any, Dict, List

from config import (
    EMAIL_API_KEY,
    EMAIL_DOMAIN,
    MAILGUN_URL,
    USE_EMAIL,
)
from membership.database.models import Email, Member
from .email_connector import EmailConnector


class MailgunEmailConnector(EmailConnector):
    @overrides
    def send_member_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]],
        tag: str = '',
        add_unsubscribe_link: bool = True,
    ):
        """
        Create an email campaign from the sender using the given MailGun email template formatted
        string and substitute the recipient variables for every email key in the dict.

        NOTE: If `recipient_variables` is empty, then the email is sent to nobody, otherwise the
        email is sent to every key of the dictionary given.

        :param sender: the from field of the email to be received
        :param subject: the subject line of the email
        :param email_template: the full template with all variables / recipient variables declared
        :param recipient_variables: all the recipients of this email by email address with
                                    all variables associated with this address in the values
        """
        filtered_recipient_variables = {
            member.email_address: obj
            for member, obj in recipient_variables.items()
            if not member.do_not_email
        }
        if add_unsubscribe_link:
            email_template = (
                f"{email_template}\n\n\n"
                "to unsubscribe click here: %tag_unsubscribe_url%"
            )
        self.send_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=email_template,
            recipient_variables=filtered_recipient_variables,
            to_emails=list(filtered_recipient_variables.keys()),
        )

    @overrides
    def send_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[str, Dict[Any, str]],
        to_emails: List[str],
        tag: str = '',
    ):
        url = f'https://api.mailgun.net/v3/{EMAIL_DOMAIN}/messages'
        payload = [
            ('from', f'{sender_name} <{sender_email}>'),
            ('recipient-variables', json.dumps(recipient_variables)),
            ('subject', subject),
            ('html', email_template),
            ('o:tag', tag),
        ]
        payload.extend([("to", email) for email in to_emails])
        if USE_EMAIL:
            r = requests.post(url, data=payload, auth=("api", EMAIL_API_KEY))
            if r.status_code > 299:
                logging.error(r.text)

    @overrides
    def update_email(self, email: Email) -> str:
        """
        Create or update routes in Mailgun
        :param email: the incoming email address (should have a least one forwarding address)
        :return: the external_id from mailgun
        """
        payload = [
            ('priority', '0'),
            (
                'description',
                'Forwarding rule for {address}'.format(address=email.email_address),
            ),
            (
                'expression',
                'match_recipient("{address}")'.format(address=email.email_address),
            ),
        ]
        payload.extend(
            [
                ("action", 'forward("{address}")'.format(address=forward.forward_to))
                for forward in email.forwarding_addresses
            ]
        )
        if email.external_id:
            r = requests.put(
                MAILGUN_URL + '/' + email.external_id,
                data=payload,
                auth=('api', EMAIL_API_KEY),
            )
            if r.status_code > 299:
                raise Exception('Mailgun api failed to update email.')
            return str(email.external_id)
        else:
            r = requests.post(MAILGUN_URL, data=payload, auth=("api", EMAIL_API_KEY))
            if r.status_code > 299:
                raise Exception('Mailgun api failed to create email.')
            return str(r.json().get('route').get('id'))

    @overrides
    def remove_email(self, external_id: str):
        r = requests.delete(
            MAILGUN_URL + '/' + external_id, auth=('api', EMAIL_API_KEY)
        )
        if r.status_code > 299:
            raise Exception("Mailgun api failed to delete email.")
