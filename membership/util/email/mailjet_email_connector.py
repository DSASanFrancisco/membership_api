import logging
import re
import requests

from overrides import overrides
from typing import Any, Dict, List

from config import (
    EMAIL_API_ID,
    EMAIL_API_KEY,
    SUPPORT_EMAIL_ADDRESS,
    USE_EMAIL,
)
from membership.database.models import Email, Member
from .email_connector import EmailConnector


class MailjetEmailConnector(EmailConnector):
    @overrides
    def send_member_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[Member, Dict[str, str]],
        tag: str = '',
        add_unsubscribe_link: bool = True,
    ):
        filtered_recipient_variables = {
            member.email_address: obj
            for member, obj in recipient_variables.items()
            if not member.do_not_email
        }
        if add_unsubscribe_link:
            email_template = (
                f'{email_template}\n\n\n to unsubscribe click here: [[mj:unsub_link]]'
            )
        self.send_emails(
            sender_name=sender_name,
            sender_email=sender_email,
            subject=subject,
            email_template=email_template,
            recipient_variables=filtered_recipient_variables,
            to_emails=list(filtered_recipient_variables.keys()),
        )

    def send_emails(
        self,
        *,
        sender_name: str,
        sender_email: str,
        subject: str,
        email_template: str,
        recipient_variables: Dict[str, Dict[Any, str]],
        to_emails: List[str],
        tag: str = '',
    ):
        mailjet_url = "https://api.mailjet.com/v3/send"
        payload = {
            'FromName': sender_name,
            'FromEmail': sender_email,
            'Subject': subject,
            'Mj-TemplateLanguage': True,
            'TemplateErrorReporting': SUPPORT_EMAIL_ADDRESS,
            'TemplateErrorDeliver': True,
            'Html-part': self._parse_existing_templates(email_template),
            'Recipients': [
                {'Email': email_address, 'Vars': recipient_variables[email_address]}
                for email_address in to_emails
            ],
        }
        if USE_EMAIL:
            r = requests.post(
                mailjet_url, json=payload, auth=(EMAIL_API_ID, EMAIL_API_KEY)
            )
            if r.status_code > 299:
                logging.error(r.text)

    def _parse_existing_templates(self, template: str) -> str:
        return re.sub(r"%recipient\.(.*?)%", r"[[var:\1]]", template)

    @overrides
    def update_email(self, email: Email) -> str:
        raise NotImplementedError("Mailjet does not support forwarding rules")

    @overrides
    def remove_email(self, external_id: str):
        raise NotImplementedError("Mailjet does not support forwarding rules")
