from sqlalchemy import event
from sqlalchemy.util.langhelpers import symbol


def unmodifiable(col):
    """Utility to make a column unmodifiable."""
    if hasattr(col, 'set'):
        @event.listens_for(col, 'set')
        def unmodifiability_listener(target, value, old_value, initiator):
            if old_value != symbol('NEVER_SET') and \
                    old_value != symbol('NO_VALUE') and \
                    old_value != value:
                raise PermissionError(
                    'cannot set column "%s" in "%s" from "%s" to "%s": it is unmodifiable' % (
                        col.name, col.class_.__name__, old_value, value))

    return col
