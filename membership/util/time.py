import pytz

from config.lib import from_env
from datetime import datetime

CHAPTER_TIME_ZONE = pytz.timezone(from_env.get_str('CHAPTER_TIME_ZONE', 'America/Los_Angeles'))


def get_current_time() -> datetime:
    return datetime.now(CHAPTER_TIME_ZONE).astimezone(pytz.utc)
