import logging
import random
import string
from datetime import datetime, timedelta
from functools import wraps
from json import loads
from typing import Optional

import jwt
import requests
from flask import request, Response, jsonify

from config import (
    JWT_SECRET,
    JWT_CLIENT_ID,
    ADMIN_CLIENT_ID,
    ADMIN_CLIENT_SECRET,
    AUTH_CONNECTION,
    AUTH_URL,
    USE_AUTH,
    NO_AUTH_EMAIL,
)
from config import PORTAL_URL
from membership.database.base import Session
from membership.database.models import AuthToken, Member
from membership.models import AuthContext, UnauthorizedException

logger = logging.getLogger(__name__)

PASSWORD_CHARS = string.ascii_letters + string.digits


class CreateAuth0UserError(Exception):

    def __init__(self, status_code: int, content: str):
        self.status_code = status_code
        self.content = content
        self.message = f"Failed to create user. Received status={status_code} content={content}"


def deny(reason: str = "") -> Response:
    """Sends a 401 response that enables basic auth"""
    response = jsonify({"status": "error", "err": reason})
    response.status_code = 401
    return response


def requires_auth(*roles: str):
    """ This defines a decorator which when added to a route function in flask requires authorization to
    view the route. Authorization includes being logged in and having the roles listed.

    example: @requires_auth('admin') requires the 'admin' user role.

    @param roles: list of role names as strings.
    """

    def decorator(f):
        @wraps(f)
        def decorated(*args, **kwargs):
            # Find our auth_token_id.
            auth_token_id = None
            if getattr(request, "form", None) and "auth_token_id" in request.form:
                auth_token_id = str(request.form["auth_token_id"])
            elif request.is_json:
                json_body = request.get_json(silent=True, force=True)
                if json_body is not None and type(json_body) is dict:
                    auth_token_id = json_body.get("auth_token_id")

            # Find and validate our requestor's email.
            session = Session()
            if USE_AUTH and auth_token_id:
                # Validate our auth token.
                auth_token = session.query(AuthToken).filter_by(id=auth_token_id)
                if not auth_token:
                    return deny("invalid request auth token (does not exist)")
                if auth_token.single_use and auth_token.used:
                    return deny("invalid request auth token (already used)")
                if auth_token.expire_date and (
                    auth_token.expire_date.timestamp() <= datetime.now().timestamp()
                ):
                    return deny("invalid request auth token (expired)")
                if auth_token.method != request.method:
                    return deny("invalid request auth token (incorrect method)")
                if auth_token.path != request.path:
                    return deny("invalid request auth token (incorrect path)")
                if auth_token.request_json_params:
                    request_json = request.get_json()
                    for key, value in jsonify(
                        auth_token.request_json_params
                    ).iteritems():
                        if request_json[key] != value:
                            return deny('invalid request (incorrect "%s" param)' % key)

                # Get our member session.
                member = session.query(Member)
                if not member:
                    return deny("invalid request auth token (member unknown)")

                if auth_token.single_use:
                    auth_token.used = True
                    session.add(auth_token)
                    session.flush()

                email = member.email_address

            elif USE_AUTH:
                # Validate our auth0 headers.
                auth = request.headers.get("authorization")
                if not auth:
                    return deny("Authorization not found.")
                token = auth.split()[1]
                try:
                    token = jwt.decode(token, JWT_SECRET, audience=JWT_CLIENT_ID)
                except Exception as e:
                    return deny(str(e))
                email = token.get("email")

            else:
                # No validation.
                email = NO_AUTH_EMAIL

            # Derive a complete AuthContext and forward to our request.
            try:
                member = session.query(Member).filter_by(email_address=email).one()
                ctx = AuthContext(member, session)
                for role in roles:
                    ctx.az.verify_role(role)
                kwargs["ctx"] = ctx
                return f(*args, **kwargs)
            except UnauthorizedException:
                return deny("not enough access")
            finally:
                session.close()

        return decorated

    return decorator


current_token = {}


def get_auth0_token() -> str:
    if not current_token or datetime.now() > current_token["expiry"]:
        current_token.update(generate_auth0_token())
    return current_token["token"]


def generate_auth0_token() -> dict:
    payload = {
        "grant_type": "client_credentials",
        "client_id": ADMIN_CLIENT_ID,
        "client_secret": ADMIN_CLIENT_SECRET,
        "audience": AUTH_URL + "api/v2/",
    }
    response = requests.post(AUTH_URL + "oauth/token", json=payload).json()
    return {
        "token": response["access_token"],
        "expiry": datetime.now() + timedelta(seconds=response["expires_in"]),
    }


def get_auth0_email_verification_link(user_id: int) -> str:
    headers = {"Authorization": "Bearer " + get_auth0_token()}

    # get a password change URL
    payload = {"result_url": PORTAL_URL, "user_id": user_id}
    r = requests.post(
        AUTH_URL + "api/v2/tickets/password-change", json=payload, headers=headers
    )
    if r.status_code > 299:
        msg = "Failed to get password url. " "Received status={} content={}".format(
            r.status_code, r.content
        )
        raise Exception(msg)
    reset_url = r.json()["ticket"]

    # get email verification link
    payload = {"result_url": reset_url, "user_id": user_id}
    r = requests.post(
        AUTH_URL + "api/v2/tickets/email-verification", json=payload, headers=headers
    )
    if r.status_code > 299:
        msg = "Failed to get verify url. " "Received status={} content={}".format(
            r.status_code, r.content
        )
        raise Exception(msg)
    validate_url = r.json()["ticket"]
    return validate_url


def create_auth0_user(email: str) -> Optional[str]:
    """
    Create the Auth0 user for the given email address.

    @returns None if a user with that email address already exists,
             otherwise the email verification url.
    """
    if not USE_AUTH:
        return PORTAL_URL

    # create the user
    payload = {
        "connection": AUTH_CONNECTION,
        "email": email,
        "password": "".join(
            random.SystemRandom().choice(PASSWORD_CHARS) for _ in range(12)
        ),
        "user_metadata": {},
        "email_verified": False,
        "verify_email": False,
    }
    headers = {"Authorization": "Bearer " + get_auth0_token()}
    r = requests.post(AUTH_URL + "api/v2/users", json=payload, headers=headers)

    if r.status_code > 299:
        logger.warning(f"Failed to create user: {r.content.decode()}")

        try:
            error_response = loads(r.content)
            if error_response["code"] == "user_exists":
                return None
        except Exception:
            # Don't handle other Auth0 issues
            logger.warning("Couldn't parse response from auth0 failure message")

        raise CreateAuth0UserError(r.status_code, r.content.decode())

    user_id = r.json()["user_id"]
    return get_auth0_email_verification_link(user_id)
