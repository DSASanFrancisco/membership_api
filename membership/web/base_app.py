from config.sentry_config import SENTRY_DSN
from flask import Flask, jsonify
from flask_cors import CORS
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

from config import USE_FEATURE_COLLECTIVE_DUES
from membership.web.util import CustomEncoder

# NOTE: Don't import from the membership package outside of this init function!
#
# Many parts of the code will reference "app" for the logger and this will cause
# cyclic dependencies of this file and the app. By putting all membership imports
# in this function, we can be sure that the app will initialize before anything
# references it.


def init(app: Flask):
    """
    Initializes the app after it has been constructed.

    Note: This is where you can import from the membership package
    """
    app.logger.info('Initializing app...')
    from membership.web.assets import asset_api
    from membership.web.members import member_api
    from membership.web.meetings import meeting_api
    from membership.web.elections import election_api
    from membership.web.email import email_api
    from membership.web.email_templates import email_templates_api
    from membership.web.email_topics import email_topics_api
    from membership.web.committees import committee_api
    from membership.web.tags import tag_api

    CORS(app)
    app.register_blueprint(asset_api)
    app.register_blueprint(member_api)
    app.register_blueprint(meeting_api)
    app.register_blueprint(election_api)
    app.register_blueprint(email_api)
    app.register_blueprint(email_templates_api)
    app.register_blueprint(email_topics_api)
    app.register_blueprint(committee_api)
    app.register_blueprint(tag_api)

    if USE_FEATURE_COLLECTIVE_DUES:
        from membership.web.payments import payments_api

        app.register_blueprint(payments_api)

    app.logger.info('App initialization complete.')


if SENTRY_DSN is not None:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        integrations=[FlaskIntegration()],

        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        # We recommend adjusting this value in production,
        traces_sample_rate=1.0
    )

app = Flask(__name__, static_folder='../../mnt/static')
app.json_encoder = CustomEncoder

init(app)


@app.route('/health', methods=["GET"])
def health_check():
    return jsonify({'health': True})
