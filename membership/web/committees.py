import dateutil
from flask import Blueprint, jsonify, request

from membership.database.models import LeadershipRole, CommitteeLeadershipRole, Committee
from membership.models import AuthContext
from membership.schemas.rest.committees import (
    get_leaders_response_schema,
    add_leadership_role_request_schema,
    add_leadership_role_response_schema,
    replace_leader_request_schema,
    replace_leader_response_schema
)
from membership.services import CommitteeService
from membership.services.committee_leadership_service import CommitteeLeadershipService
from membership.services.errors import NotFoundError, ValidationError
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound, validate_response, validate_request

committee_api = Blueprint('committee_api', __name__)
committee_service = CommitteeService()
leadership_service = CommitteeLeadershipService()


@committee_api.route('/committee/list', methods=['GET'])
@requires_auth()
def get_committees(ctx: AuthContext):
    return jsonify(committee_service.list(ctx))


@committee_api.route('/committee', methods=['POST'])
@requires_auth('admin')
def add_committee(ctx: AuthContext):
    return jsonify({
        'status': 'success',
        'created': committee_service.create(
            ctx,
            name=request.json['name'],
            provisional=request.json.get('provisional', False),
            admins=request.json.get('admin_list', []),
        )
    })


@committee_api.route('/committee/<int:committee_id>', methods=['GET'])
@requires_auth()
def get_committee(ctx: AuthContext, committee_id: int):
    committee = committee_service.get(ctx, committee_id=committee_id)
    if committee is None:
        return NotFound('Committee id={} does not exist'.format(committee_id))
    return jsonify(committee)


@committee_api.route('/committee/<int:committee_id>', methods=['PATCH'])
@requires_auth('admin')
def edit_committee(ctx: AuthContext, committee_id: int):
    committee = committee_service.get(ctx, committee_id=committee_id)
    if committee is None:
        return NotFound('Committee id={} does not exist'.format(committee_id))
    if 'inactive' not in request.json:
        return BadRequest('Only editing committee inactive status is allowed at this time')
    committee = committee_service.edit(ctx, committee_id, inactive=request.json.get('inactive'))
    return jsonify(committee)


@committee_api.route('/committee/<int:committee_id>/member_request', methods=['POST'])
@requires_auth()
def request_committee_membership(ctx: AuthContext, committee_id: int):
    result = committee_service.request_membership(
            ctx,
            committee_id=committee_id,
            member_name=ctx.requester.name,
            member_email=ctx.requester.email_address
    )

    if result is None:
        return BadRequest('There is no email address associated with this committee')

    return jsonify(
        {
            'status': 'success',
            'data': {
                'email_sent': result
            }
        }
    )


@committee_api.route('/committee/<int:committee_id>/leaders', methods=['GET'])
@requires_auth()
@validate_response(get_leaders_response_schema)
def get_leaders(ctx: AuthContext, committee_id):
    try:
        return jsonify(leadership_service.get_leadership_roles(ctx, committee_id))
    except NotFoundError as e:
        return NotFound(e.message)


@committee_api.route('/committee/<int:committee_id>/leaders', methods=['POST'])
@requires_auth()
@validate_request(add_leadership_role_request_schema)
@validate_response(add_leadership_role_response_schema)
def add_leadership_role(ctx: AuthContext, committee_id):
    if not request.json['role_title']:
        return BadRequest("No role_title was provided")
    return jsonify(leadership_service.add_leadership_role(
        ctx,
        committee_id,
        request.json['role_title'],
        request.json.get('term_limit_months')
    ))


@committee_api.route('/committee/<int:committee_id>/leaders/<int:leadership_id>', methods=['PATCH'])
@requires_auth()
@validate_request(replace_leader_request_schema)
@validate_response(replace_leader_response_schema)
def replace_leader(ctx: AuthContext, committee_id, leadership_id):
    # verify committee_id matches leadership_id (also verify leadership_id is valid)
    committee = ctx.session.query(Committee).get(committee_id)
    if committee is None:
        return NotFound(f'Committee with id {committee_id} not found')
    committee_leadership_role = ctx.session.query(CommitteeLeadershipRole).get(leadership_id)
    if committee_leadership_role is None:
        return NotFound(f'Committee leadership role with id {leadership_id} not found')
    if committee_leadership_role.committee_id != committee_id:
        return BadRequest(
            f"Committee leadership role {leadership_id} isn't part of committee {committee_id}"
        )
    try:
        term_start_date = None
        if 'term_start_date' in request.json:
            term_start_date = dateutil.parser.parse(request.json['term_start_date'])
        term_end_date = None
        if 'term_end_date' in request.json:
            term_end_date = dateutil.parser.parse(request.json['term_end_date'])
        return jsonify(leadership_service.replace_leader(
            ctx,
            leadership_id,
            member_id=request.json.get('member_id'),
            term_start_date=term_start_date,
            term_end_date=term_end_date,
        ))
    except NotFoundError as e:
        return NotFound(e.message)
    except ValidationError as e:
        return BadRequest(e.message)


@committee_api.route('/committee-leadership-roles', methods=['GET'])
@requires_auth()
def get_role_titles(ctx: AuthContext):
    roles = ctx.session.query(LeadershipRole).all()
    return jsonify([role.title for role in roles])
