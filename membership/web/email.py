from flask import Blueprint, jsonify, make_response, request

from membership.models import AuthContext
from membership.schemas.rest.emails import format_email
from membership.web.auth import requires_auth
from membership.web.util import BadRequest, NotFound, Ok
from membership.services.email_service import EmailExistsError, EmailService

email_api = Blueprint('email_api', __name__)
email_service = EmailService()


@email_api.route('/emails', methods=['GET'])
@requires_auth('admin')
def list_email_addresses(ctx: AuthContext):
    return jsonify([format_email(e) for e in email_service.list(ctx)])


@email_api.route('/emails', methods=['POST'])
@requires_auth('admin')
def create_email_address(ctx: AuthContext):
    email_address = request.json['email_address']
    forwarding_addresses = request.json['forwarding_addresses']
    try:
        email = email_service.create(
            ctx=ctx,
            email_address=email_address,
            forwarding_addresses=forwarding_addresses,
        )
    except EmailExistsError:
        return BadRequest('Email address already exists')

    return make_response(jsonify({'status': 'success', 'email': format_email(email)}), 201)


@email_api.route('/emails/<int:email_id>', methods=['PUT'])
@requires_auth('admin')
def update_email_address(ctx: AuthContext, email_id: int):
    email = email_service.update(
        ctx=ctx,
        email_id=email_id,
        email_address=request.json.get('email_address', None),
        forwarding_addresses=request.json.get('forwarding_addresses', None),
    )
    if not email:
        return NotFound('No email with that id')
    return jsonify({'status': 'success', 'email': format_email(email)})


@email_api.route('/emails/<int:email_id>', methods=['DELETE'])
@requires_auth('admin')
def delete_email_address(ctx: AuthContext, email_id: int):
    email = email_service.delete(ctx=ctx, email_id=email_id)
    if not email:
        return NotFound('No email with that id')
    return Ok()
