from flask import jsonify, Blueprint
from sqlalchemy.orm import joinedload

from membership.database.models import Tag, Member
from membership.models import AuthContext
from membership.schemas.rest.members import format_member_basics
from membership.schemas.rest.tags import format_tag
from membership.web.auth import requires_auth
from membership.web.members import member_api
from membership.web.util import NotFound, Ok

tag_api = Blueprint('tag_api', __name__)


@tag_api.route("/tags", methods=["GET"])
@requires_auth("admin")
def get_tags(ctx: AuthContext):
    tags = ctx.session.query(Tag).all()

    return jsonify([format_tag(tag) for tag in tags])


@tag_api.route("/tags/<int:tag_id>", methods=["DELETE"])
@requires_auth("admin")
def delete_tag(tag_id: int, ctx: AuthContext):
    tag = ctx.session.query(Tag).get(tag_id)

    if not tag:
        return NotFound("Tag not found.")

    # This will cascade delete the tag from the member_tags tables for all members with the tag.
    ctx.session.delete(tag)
    ctx.session.commit()

    return Ok()


@member_api.route("/tags/<int:tag_id>/members", methods=["GET"])
@requires_auth("admin")
def get_members_with_tag(tag_id: int, ctx: AuthContext):
    tag = ctx.session.query(Tag) \
        .options(
            joinedload(Tag.members),
            joinedload(Tag.members).joinedload(Member.phone_numbers),
            joinedload(Tag.members).joinedload(Member.additional_email_addresses),
            joinedload(Tag.members).joinedload(Member.all_roles)) \
        .filter(Tag.id == tag_id) \
        .first()

    if not tag:
        return NotFound("Tag not found.")

    return jsonify([format_member_basics(member, ctx.az) for member in tag.members])
