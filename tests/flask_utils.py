import json

from typing import Any, Dict
from werkzeug.test import Client


def get_json(app: Client, endpoint: str):
    return app.get(endpoint, content_type='application/json')


def post_json(app: Client, endpoint: str, payload: Dict[Any, Any] = None):
    return app.post(endpoint, data=_encode_payload(payload), content_type='application/json')


def patch_json(app: Client, endpoint: str, payload: Dict[Any, Any] = None):
    return app.patch(endpoint, data=_encode_payload(payload), content_type='application/json')


def put_json(app: Client, endpoint: str, payload: Dict[Any, Any] = None):
    return app.put(endpoint, data=_encode_payload(payload), content_type='application/json')


def delete_json(app: Client, endpoint: str, payload: Dict[Any, Any] = None):
    return app.delete(endpoint, data=_encode_payload(payload), content_type='application/json')


def _encode_payload(payload: Dict[Any, Any]):
    return None if payload is None else json.dumps(payload)
