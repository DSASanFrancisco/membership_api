import io
from typing import IO


class NullIO(io.StringIO):
    def write(self, *args):
        pass


devnull: IO[str] = NullIO()
