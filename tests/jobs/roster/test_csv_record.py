from unittest import TestCase

from jobs.roster.csv_record import CSVRecord
from jobs.roster.import_results import ImportResults
from membership.database.models import IdentityProviders


class TestCSVRecord(TestCase):

    def test_record(self):
        return CSVRecord(
            ak_id="id",
            dsa_id="id2",
            first_name="Karl",
            middle_name="Heinrich",
            last_name="Marx",
            email="karl@dsasf.org",
            active=False,
            address_line_1=None,
            address_line_2=None,
            city=None,
            zipcode=None,
            country=None,
            do_not_call=True,
            join_date=None,
            dues_paid_until=None,
            home_nums=None,
            cell_nums=None,
            work_nums=None,
            union_member=None,
            union_name=None,
            union_local=None,
        )

    def test_get_identities_should_map_csv_record_to_ak_id(self):
        ak_id = "some id"
        member_id = 123

        rec = self.test_record()
        rec.ak_id = ak_id
        ids = rec.get_identities(member_id=member_id)

        assert ids[0].member_id == member_id
        assert ids[0].provider_name == IdentityProviders.AK
        assert ids[0].provider_id == ak_id

    def test_get_identities_should_map_csv_record_to_dsa_id(self):
        dsa_id = "some id"
        member_id = 123

        rec = self.test_record()
        rec.dsa_id = dsa_id
        ids = rec.get_identities(member_id=member_id)

        assert ids[1].member_id == member_id
        assert ids[1].provider_name == IdentityProviders.DSA
        assert ids[1].provider_id == dsa_id

    def test_get_phone_numbers_should_map_csv_record_to_phone_number(self):
        home_num = "555-111-1111"
        cell_num = "555-222-2222"
        work_num = "555-333-3333"
        member_id = 123

        rec = self.test_record()
        rec.home_nums = [home_num]
        rec.cell_nums = [cell_num]
        rec.work_nums = [work_num]

        numbers = rec.get_phone_numbers(member_id, ImportResults())

        assert len(numbers) == 3

        cell = next(iter(n for n in numbers if n.name == "Cell"), None)
        assert cell
        assert cell.member_id == member_id
        assert cell.number == "+1 " + cell_num

        home = next(iter(n for n in numbers if n.name == "Home"), None)
        assert home.member_id == member_id
        assert home.number == "+1 " + home_num

        work = next(iter(n for n in numbers if n.name == "Work"), None)
        assert work.member_id == member_id
        assert work.number == "+1 " + work_num

    def test_get_phone_numbers_should_skip_empty_strings(self):
        rec = self.test_record()
        rec.home_nums = [""]
        rec.cell_nums = [""]
        rec.work_nums = [""]

        numbers = rec.get_phone_numbers(123, ImportResults())

        assert len(numbers) == 0

    def test_get_phone_numbers_should_skip_duplicates(self):
        rec = self.test_record()
        rec.home_nums = ["555-111-1111"]
        rec.cell_nums = ["555-111-1111"]
        rec.work_nums = ["555-111-1111"]

        numbers = rec.get_phone_numbers(123, ImportResults())

        assert len(numbers) == 1

    def test_get_phone_numbers_should_skip_errors_and_report(self):
        rec = self.test_record()
        rec.cell_nums = ["invalid"]
        rec.work_nums = []
        rec.home_nums = []

        results = ImportResults()
        numbers = rec.get_phone_numbers(123, results)

        assert len(numbers) == 0
        assert next(iter(results.phone_numbers_failed), None) == "invalid"

    def test_parse_zipcode_should_handle_9_digit_codes(self):
        assert CSVRecord.parse_zipcode("123456789") == "12345-6789"

    def test_format_date_handle_multiple_formats(self):
        assert CSVRecord.format_optional_date("2020-01-01")
        assert CSVRecord.format_optional_date("01/01/2020")
