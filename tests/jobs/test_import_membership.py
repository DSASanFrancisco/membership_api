from datetime import datetime
from unittest.mock import MagicMock

import pytz
from dateutil.relativedelta import relativedelta
from freezegun import freeze_time

from jobs.auth0_invites import SendAuth0Invites
from jobs.import_membership import ImportNationalMembership
from jobs.roster.csv_record import CSVRecord
from jobs.roster.session_cache import RosterImportSessionCache
from membership.database.base import metadata, engine, Session
from membership.database.models import Member, NationalMembershipData, PhoneNumber, \
    Identity, IdentityProviders, AdditionalEmailAddress, Role


class TestImportMembership:

    def setup(self):
        metadata.create_all(engine)

    def teardown(self) -> None:
        metadata.drop_all(engine)

    def new_member(self) -> (CSVRecord, None, None):
        ak_id = "33729"
        dsa_id = "6729"
        home_number = "123-456-7890"
        cell_number = "234-567-8901"
        work_number = "345-678-9012"

        csv = CSVRecord(
            ak_id=ak_id,
            dsa_id=dsa_id,
            first_name="Karl",
            middle_name="Heinrich",
            last_name="Marx",
            email="karl@dsasf.org",
            active=True,
            address_line_1="123 Test Lane",
            address_line_2="Apt 3",
            city="SF",
            zipcode="94117",
            country="USA",
            do_not_call=False,
            join_date=datetime(1990, 10, 1).astimezone(pytz.utc),
            dues_paid_until=datetime(2020, 10, 1).astimezone(pytz.utc),
            home_nums=[home_number],
            cell_nums=[cell_number],
            work_nums=[work_number],
            # TODO: Add tests for these
            union_member=None,
            union_name=None,
            union_local=None,
        )

        return csv, None, None

    def existing_member(
            self,
            include_general_member_role: bool = True,
            match_email: bool = False) -> (CSVRecord, NationalMembershipData, Member):
        ak_id = "33729"
        dsa_id = "6729"
        member_id = 123
        home_number = "223-456-7890"
        cell_number = "234-567-8901"
        work_number = "345-678-9012"

        csv = CSVRecord(
            ak_id=ak_id,
            dsa_id=dsa_id,
            first_name="Karl",
            middle_name="Heinrich",
            last_name="Marx",
            email="karl@dsasf.org",
            active=True,
            address_line_1="123 Test Lane",
            address_line_2="Apt 3",
            city="SF",
            zipcode="94117",
            country="USA",
            do_not_call=False,
            join_date=datetime(1990, 10, 1).astimezone(pytz.utc),
            dues_paid_until=datetime(2020, 10, 1).astimezone(pytz.utc),
            home_nums=[home_number],
            cell_nums=[cell_number],
            work_nums=[work_number],
            # TODO: Add tests for these
            union_member=None,
            union_name=None,
            union_local=None,
        )

        national = NationalMembershipData(
            member_id=member_id,
            active=False,
            ak_id=ak_id,
            dsa_id=dsa_id,
            do_not_call=False,
            first_name="Carl",
            middle_name="Hienrick",
            last_name="Marks",
            address_line_1="bad addr",
            address_line_2="bad addr 2",
            city="Vancouver",
            country="CAN",
            zipcode="12345",
            join_date=datetime(2000, 10, 1).astimezone(pytz.utc),
            dues_paid_until=datetime(2010, 10, 1).astimezone(pytz.utc)
        )

        ids = [
            Identity(member_id=member_id, provider_name=IdentityProviders.AK, provider_id=ak_id),
            Identity(member_id=member_id, provider_name=IdentityProviders.DSA, provider_id=dsa_id)
        ]
        numbers = [
            PhoneNumber(member_id=member_id, name="Home", number="+1 " + home_number),
            PhoneNumber(member_id=member_id, name="Cell", number="+1 " + cell_number),
            PhoneNumber(member_id=member_id, name="Work", number="+1 " + work_number)
        ]
        addl_emails = [
            AdditionalEmailAddress(member_id=member_id, email_address="karl2@dsasf.org")
        ]
        roles = [
            Role(member_id=member_id, role="member")
        ]

        chapter = Member(
            id=member_id,
            first_name="Karl",
            last_name="",
            email_address="karl@dsasf.org" if match_email else "carl@dsasf.org",
            normalized_email="karl@dsasf.org" if match_email else "carl@dsasf.org",
            do_not_call=False,
            do_not_email=False,
            phone_numbers=numbers,
            additional_email_addresses=addl_emails,
            onboarded_on=datetime(2000, 10, 1).astimezone(pytz.utc),
            identities=ids,
            all_roles=roles if include_general_member_role else []
        )

        return csv, national, chapter

    @freeze_time("2020-10-01")
    def test_import_all_should_create_national_record_if_no_links(self):
        csv_record, _, _ = self.new_member()

        job = ImportNationalMembership()

        cache = RosterImportSessionCache(lambda: Session())

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.memberships_created) == 1

        session = Session()
        membership = session.query(NationalMembershipData) \
            .filter(NationalMembershipData.ak_id == csv_record.ak_id) \
            .first()
        assert membership
        assert membership.dsa_id == csv_record.dsa_id
        assert membership.do_not_call == csv_record.do_not_call
        assert membership.active == csv_record.active
        assert membership.first_name == csv_record.first_name
        assert membership.middle_name == csv_record.middle_name
        assert membership.last_name == csv_record.last_name
        assert membership.address_line_1 == csv_record.address_line_1
        assert membership.address_line_2 == csv_record.address_line_2
        assert membership.city == csv_record.city
        assert membership.country == csv_record.country
        assert membership.zipcode == csv_record.zipcode
        assert membership.join_date == csv_record.join_date
        assert membership.dues_paid_until == csv_record.dues_paid_until

        assert len(results.members_created) == 1

        member = session.query(Member) \
            .filter(Member.email_address == csv_record.email) \
            .first()
        assert membership.member_id
        assert member.id
        assert membership.member_id == member.id

    @freeze_time("2020-10-01")
    def test_import_all_should_update_national_record_if_linked(self):
        csv_record, nat_record, chapter_record = self.existing_member(match_email=True)

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.memberships_updated) == 1

        membership = Session().query(NationalMembershipData) \
            .filter(NationalMembershipData.ak_id == csv_record.ak_id) \
            .first()
        assert membership

        assert membership.dsa_id == csv_record.dsa_id
        assert membership.do_not_call == csv_record.do_not_call
        assert membership.active == csv_record.active
        assert membership.first_name == csv_record.first_name
        assert membership.middle_name == csv_record.middle_name
        assert membership.last_name == csv_record.last_name
        assert membership.address_line_1 == csv_record.address_line_1
        assert membership.address_line_2 == csv_record.address_line_2
        assert membership.city == csv_record.city
        assert membership.country == csv_record.country
        assert membership.zipcode == csv_record.zipcode
        assert membership.join_date == csv_record.join_date
        assert membership.dues_paid_until == csv_record.dues_paid_until

    @freeze_time("2020-10-01")
    def test_import_all_should_create_chapter_record_if_no_links(self):
        csv_record, _, _ = self.new_member()

        job = ImportNationalMembership()

        cache = RosterImportSessionCache(lambda: Session())

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.members_created) == 1

        member = Session().query(Member) \
            .filter(Member.email_address == csv_record.email) \
            .first()
        assert member
        assert member.first_name == csv_record.first_name
        assert member.last_name == csv_record.last_name
        assert member.normalized_email == csv_record.email
        assert member.do_not_call == csv_record.do_not_call
        assert not member.do_not_email
        assert not member.dues_paid_overridden_on
        assert not member.suspended_on
        assert not member.left_chapter_on
        assert member.onboarded_on >= datetime.now(pytz.utc) - relativedelta(minutes=+2)

    @freeze_time("2020-10-01")
    def test_import_all_should_update_chapter_record_if_linked(self):
        csv_record, nat_record, chapter_record = self.existing_member()
        csv_record.email = "carl@dsasf.org"

        orig_onboarded_on = chapter_record.onboarded_on

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.members_updated) == 1

        member = session.query(Member) \
            .filter(Member.email_address == csv_record.email) \
            .first()

        assert member
        assert member.first_name == csv_record.first_name
        assert member.last_name == csv_record.last_name
        assert member.normalized_email == csv_record.email
        assert member.do_not_call == csv_record.do_not_call
        assert not member.do_not_email
        assert not member.dues_paid_overridden_on
        assert not member.suspended_on
        assert not member.left_chapter_on
        assert member.onboarded_on == orig_onboarded_on

    @freeze_time("2020-10-01")
    def test_import_all_should_link_based_on_first_and_last_name_if_email_doesnt_match(self):
        csv_record, nat_record, chapter_record = self.existing_member()
        csv_record.email = "bademail@gmail.com"
        chapter_record.first_name = "Karl"
        chapter_record.last_name = "Marx"
        chapter_record.do_not_call = True

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()
        session.is_modified = MagicMock(return_value=True)

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: session)

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.members_updated) == 1

    @freeze_time("2020-10-01")
    def test_import_all_should_link_based_on_additional_email(self):
        csv_record, nat_record, chapter_record = self.existing_member()
        csv_record.email = "karl2@dsasf.org"

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.members_updated) == 1

    @freeze_time("2020-10-01")
    def test_import_all_should_not_link_records_with_emails_prefixed_DELETE(self):
        csv_record, nat_record, chapter_record = self.existing_member()
        chapter_record.email_address = "DELETE_bademail@gmail.com"
        chapter_record.first_name = "Karl"
        chapter_record.last_name = "Marx"

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.members_updated) == 0

    def test_import_all_should_send_auth0_invite_if_new_member(self):
        csv_record, _, _ = self.new_member()

        job = ImportNationalMembership()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        job.import_all([csv_record], cache, auth0)

        auth0.send_invite.assert_called_once()

    @freeze_time("2020-10-01")
    def test_import_all_should_link_based_on_member_role_if_ambiguous(self):
        csv_record, nat_record, chapter_record = self.existing_member(match_email=True)

        ambiguous_chapter_record = Member(
            id=456,
            first_name="Karl",
            last_name="Marx",
            email_address="carl@dsasf.org",
            normalized_email="carl@dsasf.org",
            do_not_call=False,
            do_not_email=False,
            phone_numbers=[],
            additional_email_addresses=[],
            onboarded_on=datetime(2000, 10, 1).astimezone(pytz.utc),
            identities=[],
            all_roles=[]
        )

        session = Session()
        session.add_all([nat_record, chapter_record, ambiguous_chapter_record])
        session.commit()

        job = ImportNationalMembership()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        results = job.import_all(
            [csv_record],
            cache,
            auth0)

        assert len(results.members_updated) == 1

        updated_id = next(iter(results.members_updated))
        assert updated_id == chapter_record.id

    @freeze_time("2020-10-01")
    def test_import_all_should_preserve_do_not_call_if_true(self):
        csv_record, nat_record, chapter_record = self.existing_member()
        csv_record.email = "carl@dsasf.org"
        csv_record.do_not_call = False
        nat_record.do_not_call = True
        chapter_record.do_not_call = True

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        job.import_all([csv_record], cache, auth0)

        member = session.query(Member).get(chapter_record.id)
        assert member
        assert member.do_not_call

        membership = session.query(NationalMembershipData).get(nat_record.id)
        assert membership
        assert membership.do_not_call

    @freeze_time("2020-10-01")
    def test_import_all_should_replace_first_name_if_defined(self):
        csv_record, nat_record, chapter_record = self.existing_member(match_email=True)

        chapter_first_name = ""
        chapter_record.first_name = chapter_first_name

        nat_first_name = "Karl"
        nat_record.first_name = nat_first_name

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        job.import_all([csv_record], cache, auth0)

        member = session.query(Member).get(chapter_record.id)
        assert member
        assert member.first_name == nat_first_name

    @freeze_time("2020-10-01")
    def test_import_all_should_preserve_first_name_if_defined(self):
        csv_record, nat_record, chapter_record = self.existing_member(match_email=True)

        chapter_first_name = "Karl"
        chapter_record.first_name = chapter_first_name

        nat_first_name = "Carl"
        nat_record.first_name = nat_first_name

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        job.import_all([csv_record], cache, auth0)

        member = session.query(Member).get(chapter_record.id)
        assert member
        assert member.first_name == chapter_first_name

    @freeze_time("2020-10-01")
    def test_import_all_should_replace_last_name_if_empty(self):
        csv_record, nat_record, chapter_record = self.existing_member(match_email=True)

        chapter_last_name = ""
        chapter_record.last_name = chapter_last_name

        nat_last_name = "Marx"
        nat_record.last_name = nat_last_name

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        job.import_all([csv_record], cache, auth0)

        member = session.query(Member).get(chapter_record.id)
        assert member
        assert member.last_name == nat_last_name

    @freeze_time("2020-10-01")
    def test_import_all_should_preserve_last_name_if_defined(self):
        csv_record, nat_record, chapter_record = self.existing_member(match_email=True)

        chapter_last_name = "Marx"
        chapter_record.last_name = chapter_last_name

        nat_last_name = "Marks"
        nat_record.last_name = nat_last_name

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        job.import_all([csv_record], cache, auth0)

        member = session.query(Member).get(chapter_record.id)
        assert member
        assert member.last_name == chapter_last_name

    @freeze_time("2020-10-01")
    def test_import_all_should_reset_dues_overridden_if_dues_changed(self):
        csv_record, nat_record, chapter_record = self.existing_member(False)
        csv_record.email = "carl@dsasf.org"
        csv_record.dues_paid_until = datetime(2020, 11, 1)
        nat_record.dues_paid_until = datetime(2020, 10, 1)
        chapter_record.dues_paid_overridden_on = datetime(2020, 10, 1)

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        job.import_all([csv_record], cache, auth0)

        assert not chapter_record.dues_paid_overridden_on

    @freeze_time("2020-10-01")
    def test_import_all_should_add_new_phone_number(self):
        csv_record, nat_record, chapter_record = self.existing_member(False)
        csv_record.email = "carl@dsasf.org"
        csv_record.home_nums.append("333-444-5555")

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.phone_numbers_added) == 1

        number = next(iter(results.phone_numbers_added), None)
        assert number

        assert number.number == "+1 333-444-5555"

    @freeze_time("2020-10-01")
    def test_import_all_should_not_onboard_someone_who_was_already_onboarded(self):
        csv_record, nat_record, chapter_record = self.existing_member(False)
        csv_record.email = "carl@dsasf.org"
        chapter_record.onboarded_on = datetime(2020, 1, 1)

        job = ImportNationalMembership()

        session = Session()
        session.add_all([nat_record, chapter_record])
        session.commit()

        auth0 = SendAuth0Invites()
        auth0.send_invite = MagicMock(return_value=None)

        cache = RosterImportSessionCache(lambda: Session())

        results = job.import_all([csv_record], cache, auth0)

        assert len(results.members_onboarded) == 0
