from datetime import datetime

from config import SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import Attendee, Meeting, Member, Role
from membership.repos import MeetingRepo


class TestMeetingRepo:
    @classmethod
    def setup_class(cls):
        metadata.create_all(engine)

    @classmethod
    def teardown_class(cls):
        metadata.drop_all(engine)

    def test_all(self):
        session = Session()

        m = Member()
        m.email_address = SUPER_USER_EMAIL
        m.first_name = "Eugene"
        m.last_name = "Debs"
        session.add(m)
        role = Role()
        role.member = m
        role.role = 'admin'
        session.add(role)
        session.commit()

        member1 = Member(email_address='test+1@test.com', first_name='m1-name')
        member2 = Member(email_address='test+2@test.com', first_name='m2-name')
        meeting1 = Meeting(
            short_id=1,
            name='General Meeting 1',
            start_time=datetime(2017, 1, 1),
            end_time=datetime(2017, 1, 1),
            owner_id=m.id,
        )
        meeting2 = Meeting(
            short_id=2,
            name='General Meeting 2',
            start_time=datetime(2017, 2, 1),
            end_time=datetime(2017, 2, 1),
            owner_id=m.id,
        )
        meeting3 = Meeting(
            short_id=3,
            name='General Meeting 3',
            start_time=datetime(2017, 3, 1),
            end_time=datetime(2017, 3, 1),
            landing_url='www.meeting_agenda.com/meeting3',
            owner_id=m.id,
        )
        attendee1 = Attendee(member=member1, meeting=meeting1)
        attendee2 = Attendee(member=member2, meeting=meeting1)
        attendee3 = Attendee(member=member1, meeting=meeting2)

        session.add_all([
            member1,
            member2,
            meeting1,
            meeting2,
            meeting3,
            attendee1,
            attendee2,
            attendee3,
        ])
        session.commit()

        result = MeetingRepo(Meeting).most_recent(session)
        expected = [
            meeting1,
            meeting2,
            meeting3,
        ]

        session.close()

        assert result == expected
