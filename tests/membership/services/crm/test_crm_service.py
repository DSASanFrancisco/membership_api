from datetime import datetime
from typing import List, Dict
from unittest.mock import MagicMock

import pytz

from membership.database.models import PhoneNumber, Member
from membership.services import PhoneNumberService
from membership.services.crm.crm_record import CrmRecord
from membership.services.crm.crm_service import CrmService
from membership.services.crm.crm_sync_result import CrmSyncResult, CrmSyncStatus


class TestCrmService:

    def test_sync_member_should_insert_record_if_not_found_in_crm(self):
        crm_service = self._create_service_stub([])

        email = 'test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = False
        do_not_call = False

        crm_record = CrmRecord(
            record_id=None,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
        member = Member(
            first_name=first_name,
            last_name=last_name,
            email_address=email,
            phone_numbers=[
                PhoneNumber(number=phone, name='Cell')
            ],
            date_created=datetime(2020, 1, 1).astimezone(pytz.utc),
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )

        results = crm_service.sync_member(member, source)
        assert results == CrmSyncResult(
            email=email,
            status=CrmSyncStatus.SUCCESS.name,
            insert_count=1,
            update_count=0,
            message="Added member record!"
        )
        crm_service._insert_one.assert_called_once_with(crm_record)

    def test_sync_member_should_update_record_if_found_in_crm_and_differs(self):
        record_id = 'ref1234'
        email = 'test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        new_phone = '(890)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = False
        do_not_call = False

        crm_record = CrmRecord(
            record_id=record_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
        crm_service = self._create_service_stub([crm_record])

        member = Member(
            first_name=first_name,
            last_name=last_name,
            email_address=email,
            phone_numbers=[
                PhoneNumber(number=new_phone, name='Cell')
            ],
            date_created=datetime(2020, 1, 1).astimezone(pytz.utc),
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
        updated_crm_record = CrmRecord(
            record_id=record_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=new_phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )

        results = crm_service.sync_member(member, source)
        assert results == CrmSyncResult(
            email=email,
            status=CrmSyncStatus.SUCCESS.name,
            insert_count=0,
            update_count=1,
            message="Updated 1 record(s)."
        )
        crm_service._batch_update.assert_called_once_with([updated_crm_record])

    def test_sync_member_should_not_update_record_if_found_in_crm_but_does_not_differ(self):
        record_id = 'ref1234'
        email = 'test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = False
        do_not_call = False

        crm_record = CrmRecord(
            record_id=record_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
        crm_service = self._create_service_stub([crm_record])

        member = Member(
            first_name=first_name,
            last_name=last_name,
            email_address=email,
            phone_numbers=[
                PhoneNumber(number=phone, name='Cell')
            ],
            date_created=datetime(2020, 1, 1).astimezone(pytz.utc),
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )

        results = crm_service.sync_member(member, source)
        assert results == CrmSyncResult(
            email=email,
            status=CrmSyncStatus.SUCCESS.name,
            insert_count=0,
            update_count=0,
            message="Updated 0 record(s)."
        )
        crm_service._insert_one.assert_not_called()
        crm_service._batch_update.assert_called_once_with([])

    def test_bulk_sync_members_should_insert_records_if_not_found_in_crm(self):
        email = 'Test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = False
        do_not_call = False

        crm_record = CrmRecord(
            record_id=None,
            email=email.lower(),
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
        member = Member(
            first_name=first_name,
            last_name=last_name,
            email_address=email.lower(),
            phone_numbers=[
                PhoneNumber(number=phone, name='Cell')
            ],
            date_created=datetime(2020, 1, 1).astimezone(pytz.utc),
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )

        crm_service = self._create_service_stub([], {})

        results = crm_service.bulk_sync_members([member], source)
        assert results == CrmSyncResult(
            email="bulk action",
            status=CrmSyncStatus.SUCCESS.name,
            insert_count=1,
            update_count=0,
            message="Inserted 1 record(s). Updated 0 record(s)."
        )
        crm_service._batch_inserts.assert_called_once_with([crm_record])
        crm_service._batch_update.assert_not_called()

    def test_bulk_sync_members_should_update_records_if_found_in_crm(self):
        record_id = 'ref1234'
        email = 'test@example.com'
        first_name = 'Test'
        last_name = 'User'
        phone = '(123)-456-7890'
        new_phone = '(890)-456-7890'
        source = 'website'
        join_date = '01/01/2020'
        do_not_email = False
        do_not_call = False

        crm_record = CrmRecord(
            record_id=record_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
        member = Member(
            first_name=first_name,
            last_name=last_name,
            email_address=email,
            phone_numbers=[
                PhoneNumber(number=new_phone, name='Cell')
            ],
            date_created=datetime(2020, 1, 1).astimezone(pytz.utc),
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )
        updated_crm_record = CrmRecord(
            record_id=record_id,
            email=email,
            first_name=first_name,
            last_name=last_name,
            phone=new_phone,
            onboarding_source=source,
            join_date=join_date,
            do_not_email=do_not_email,
            do_not_call=do_not_call
        )

        crm_service = self._create_service_stub([], {email: crm_record})

        results = crm_service.bulk_sync_members([member], source)
        assert results == CrmSyncResult(
            email="bulk action",
            status=CrmSyncStatus.SUCCESS.name,
            insert_count=0,
            update_count=1,
            message="Inserted 0 record(s). Updated 1 record(s)."
        )
        crm_service._batch_inserts.assert_not_called()
        crm_service._batch_update.assert_called_once_with([updated_crm_record])

    def _create_service_stub(self,
                             search_result: List[CrmRecord],
                             crm_cache: Dict[str, CrmRecord] = {}) -> CrmService:
        crm_service = CrmService(PhoneNumberService())
        crm_service._search = MagicMock(return_value=search_result)
        crm_service._get_all_records = MagicMock(return_value=crm_cache)
        crm_service._insert_one = MagicMock(return_value=None)
        crm_service._batch_update = MagicMock(return_value=None)
        crm_service._batch_inserts = MagicMock(return_value=None)

        return crm_service
