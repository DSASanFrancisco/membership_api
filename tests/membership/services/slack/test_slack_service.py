from typing import List
from unittest.mock import MagicMock

from slack_sdk.models.blocks import Block

from config.slack_config import SLACK_ONBOARDING_CHANNEL_ID
from membership.database.models import Member
from membership.services.onboarding.onboarding_results import OnboardingResults, OnboardingResult
from membership.services.onboarding.onboarding_status import OnboardingStatus
from membership.services.slack.slack_service import SlackService


class SlackClientStub:

    def chat_postMessage(self, channel: str, text: str, blocks: List[Block]) -> None:
        self.channel = channel
        self.text = text
        self.blocks = blocks


class TestSlackService:

    def test_notify_individual_sign_up(self):
        member = Member(
            id=1,
            first_name="Test",
            last_name="User",
            email_address="test@example.com",
            do_not_email=False,
            do_not_call=False,
        )

        onboarding_source = "Public Website"

        onboarding_results = OnboardingResults(
            auth0_result=OnboardingResult(OnboardingStatus.SUCCESS),
            portal_email_result=OnboardingResult(OnboardingStatus.SUCCESS),
            google_group_result=OnboardingResult(OnboardingStatus.SUCCESS),
            slack_result=OnboardingResult(OnboardingStatus.SUCCESS),
            newsletter_result=OnboardingResult(OnboardingStatus.SUCCESS),
            hustle_result=OnboardingResult(OnboardingStatus.SUCCESS),
            crm_result=OnboardingResult(OnboardingStatus.SUCCESS)
        )

        stub = SlackClientStub()

        service = SlackService()
        service._get_slack_client = MagicMock(return_value=stub)

        service.notify_of_individual_sign_up(member, onboarding_source, onboarding_results)

        assert stub.channel == SLACK_ONBOARDING_CHANNEL_ID
        assert stub.text == "A new member has signed up!"
        assert stub.blocks

    def test_create_result_block_for_success(self):
        result = OnboardingResult(OnboardingStatus.SUCCESS)

        text = SlackService()._create_result_block("Auth0", result)

        assert text == "- *Auth0:* Success!"

    def test_create_result_block_for_already_added(self):
        result = OnboardingResult(OnboardingStatus.ALREADY_ADDED)

        text = SlackService()._create_result_block("Auth0", result)

        assert text == "- *Auth0:* Already Added"

    def test_create_result_block_for_opted_out(self):
        result = OnboardingResult(OnboardingStatus.OPTED_OUT)

        text = SlackService()._create_result_block("Auth0", result)

        assert text == "- *Auth0:* Opted Out"

    def test_create_result_block_for_error(self):
        result = OnboardingResult(OnboardingStatus.ERROR, 'test error')

        text = SlackService()._create_result_block("Auth0", result)

        assert text == "- *Auth0:* Error - test error"

    def test_create_result_block_for_not_implemented(self):
        result = OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED)

        text = SlackService()._create_result_block("Auth0", result)

        assert not text

    def test_create_next_step_for_error(self):
        result = OnboardingResult(OnboardingStatus.ERROR)

        text = SlackService()._create_next_step_block("Auth0", result)

        assert text == "- Add to Auth0"

    def test_create_next_step_for_not_implemented(self):
        result = OnboardingResult(OnboardingStatus.NOT_IMPLEMENTED)

        text = SlackService()._create_next_step_block("Auth0", result)

        assert text == "- Add to Auth0"

    def test_create_next_step_ignores_success(self):
        result = OnboardingResult(OnboardingStatus.SUCCESS)

        text = SlackService()._create_next_step_block("Auth0", result)

        assert not text

    def test_create_next_step_already_added(self):
        result = OnboardingResult(OnboardingStatus.ALREADY_ADDED)

        text = SlackService()._create_next_step_block("Auth0", result)

        assert not text

    def test_create_next_step_opted_out(self):
        result = OnboardingResult(OnboardingStatus.OPTED_OUT)

        text = SlackService()._create_next_step_block("Auth0", result)

        assert not text
