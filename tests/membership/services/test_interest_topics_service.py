from typing import List

from membership.database.base import engine, metadata, Session
from membership.database.models import Interest, InterestTopic
from membership.models import Member
from membership.models.authz import AuthContext
from membership.services import InterestTopicsService


class TestInterestTopicsService:
    interest_topics_service = InterestTopicsService()

    def setup_method(cls, method):
        metadata.create_all(engine)

    def teardown_method(cls, method):
        metadata.drop_all(engine)

    def test_create_and_list_interests(self):
        session = Session()
        member1 = Member(id=1)
        member2 = Member(id=2)
        member1.email_address = "emma.goldman@riseup.net"
        member2.email_address = "du.bois@gmail.com"
        session.add(member1)
        session.add(member2)

        ctx = AuthContext(member1, session)

        assert self.interest_topics_service.list_all_interest_topics(ctx) == []
        assert ctx.session.query(Interest).all() == []

        def _verify_topics(topics: List[InterestTopic], names: List[str]):
            return set(t.name for t in topics) == set(names) and len(topics) == len(
                names
            )

        # Create interests for member1. Corresponding topics should be generated.
        interests = self.interest_topics_service.create_interests(
            ctx, member1.id, {"tech", "housing"}
        )
        assert len(interests) == 2

        all_topics = self.interest_topics_service.list_all_interest_topics(ctx)
        assert _verify_topics(all_topics, ["tech", "housing"])
        assert len(ctx.session.query(Interest).all()) == 2

        # Create overlapping interests for member2. Topics that do not already exist
        # should be generated.
        interests = self.interest_topics_service.create_interests(
            ctx, member2.id, {"housing", "feminism"}
        )
        assert len(interests) == 2

        all_topics = self.interest_topics_service.list_all_interest_topics(ctx)
        assert _verify_topics(all_topics, ["tech", "housing", "feminism"])
        assert len(ctx.session.query(Interest).all()) == 4

        topics1 = self.interest_topics_service.list_member_interest_topics(ctx, member1)
        assert _verify_topics(topics1, ["tech", "housing"])

        topics2 = self.interest_topics_service.list_member_interest_topics(ctx, member2)
        assert _verify_topics(topics2, ["housing", "feminism"])

        housing_topic = (
            ctx.session.query(InterestTopic)
            .filter(InterestTopic.name == 'housing')
            .one_or_none()
        )
        assert housing_topic is not None

        housing_interested_members = self.interest_topics_service.list_interest_topic_members(
            ctx, housing_topic
        )
        expected_housing_emails = {member1.email_address, member2.email_address}
        actual_housing_emails = {m.email_address for m in housing_interested_members}
        assert actual_housing_emails == expected_housing_emails
