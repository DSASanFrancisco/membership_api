from datetime import datetime

from membership.database.base import engine, metadata, Session
from membership.database.models import Member, Role, PhoneNumber, NationalMembershipData
from membership.services import PhoneNumberService


class TestPhoneNumberService:
    def setup(self):
        metadata.create_all(engine)
        session = Session()
        m = Member(id=1)
        m.email_address = "someone@example.com"
        m.notes = "Some notes on this member"
        session.add(m)
        role1 = Role(member=m, role="member", date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role="admin", date_created=datetime(2016, 1, 1))
        session.add_all([role1, role2])
        membership = NationalMembershipData(
            member=m,
            ak_id="73750",
            first_name="Huey",
            last_name="Newton",
            address_line_1="123 Main St",
            city="Oakland",
            country="United States",
            zipcode="94612",
            dues_paid_until=datetime(3020, 6, 1, 0),
        )
        session.add(membership)

    def teardown(self):
        metadata.drop_all(engine)

    def test_add_all_single_phone_number(self):
        session = Session()
        phone_number = PhoneNumber(member_id=1, name="Home", number="425-123-2345")
        pnservice = PhoneNumberService()
        pnservice.add_all(session, [phone_number])
        result = session.query(PhoneNumber).first()
        assert result is not None
        assert result.name == "Home"
        assert result.number == "425-123-2345"

    def test_upsert_single_phone_number(self):
        session = Session()
        phone_number = PhoneNumber(member_id=1, name="Home", number="425-123-2345")
        pnservice = PhoneNumberService()
        pnservice.upsert(session, phone_number.member_id, phone_number)
        result = session.query(PhoneNumber).first()
        assert result is not None
        assert result.name == "Home"
        assert result.number == "425-123-2345"
