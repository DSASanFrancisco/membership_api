import pytest
from datetime import datetime

from membership.database.base import engine
from membership.database.models import Member
from membership.services.search_service import process_search_query
from sqlalchemy import select
from freezegun import freeze_time


@pytest.fixture
def membership_data():
    from membership.database.base import metadata, Session
    from membership.database.models import NationalMembershipData

    metadata.create_all(engine)

    session = Session()
    m1 = Member(id=1)
    m1.email_address = "huey@example.com"
    session.add(m1)
    membership1 = NationalMembershipData(
        member=m1,
        ak_id='73750',
        first_name='Huey',
        last_name='Newton',
        address_line_1='123 Main St',
        city='Oakland',
        country='United States',
        zipcode='94612',
        join_date=datetime(2019, 6, 1, 0),
        dues_paid_until=datetime(3020, 6, 1, 0), )
    session.add(membership1)

    m2 = Member(id=2)
    m2.email_address = "test@example.com"
    session.add(m2)
    membership2 = NationalMembershipData(
        member=m2,
        ak_id='12345',
        first_name='Huey',
        last_name='Lewis',
        address_line_1='456 The News Ave',
        city='San Jose',
        country='United States',
        zipcode='94112',
        join_date=datetime(2019, 12, 15, 0),
        dues_paid_until=datetime(3020, 12, 1, 0), )
    session.add(membership2)

    m3 = Member(id=3)
    m3.email_address = "anonymous@example.com"
    session.add(m3)

    yield session

    metadata.drop_all(engine)


class TestSearchService:
    def test_blank_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "")
        expected = select([Member]).order_by(Member.id)
        assert str(actual.statement.compile(engine)) == str(expected.compile(engine))
        assert len(actual.all()) == 3

    @freeze_time("2020-01-01")
    def test_membership_new_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "is:new").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'test@example.com'

        actual = process_search_query(query, "membership:new").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'test@example.com'

    @freeze_time("3020-07-01")
    def test_membership_expired_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "is:expired").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'huey@example.com'

        actual = process_search_query(query, "membership:expired").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'huey@example.com'

    def test_membership_none_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "membership:none").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'anonymous@example.com'

    @freeze_time("2020-01-01")
    def test_membership_valid_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "membership:valid").all()
        assert len(actual) == 2
        assert actual[0].email_address == 'huey@example.com'
        assert actual[1].email_address == 'test@example.com'

    def test_full_zip_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "location:94112").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'test@example.com'

        actual = process_search_query(query, "zip:94112").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'test@example.com'

    def test_partial_zip_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "location:94").all()
        assert len(actual) == 2
        assert actual[0].email_address == 'huey@example.com'
        assert actual[1].email_address == 'test@example.com'

        actual = process_search_query(query, "zip:94").all()
        assert len(actual) == 2
        assert actual[0].email_address == 'huey@example.com'
        assert actual[1].email_address == 'test@example.com'

    def test_bad_zip_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "location:11z41").all()
        assert len(actual) == 0

        actual = process_search_query(query, "zip:11z41").all()
        assert len(actual) == 0

    def test_city_search(self, membership_data):
        query = membership_data.query(Member).order_by(Member.id)
        actual = process_search_query(query, "location:Oak").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'huey@example.com'

        actual = process_search_query(query, "city:Oak").all()
        assert len(actual) == 1
        assert actual[0].email_address == 'huey@example.com'
