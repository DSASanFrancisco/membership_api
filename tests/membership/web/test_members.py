import json
from datetime import datetime
from io import BytesIO

import pytz
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_EMAIL
from membership.database.base import engine, metadata, Session
from membership.database.models import (
    Attendee,
    Committee,
    Meeting,
    Member,
    NationalMembershipData,
    Role,
    Interest,
    InterestTopic,
    PhoneNumber,
    Tag)
from deepdiff import DeepDiff
from membership.web.base_app import app
from tests.flask_utils import delete_json, get_json, post_json, put_json, patch_json


class TestWebMembers:
    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        # set up for auth
        session = Session()

        tag1 = Tag(id=10, name="Tag 1", date_added=datetime(2020, 10, 1).astimezone(pytz.utc))
        tag2 = Tag(id=11, name="Tag 2", date_added=datetime(2020, 10, 1).astimezone(pytz.utc))

        m = Member(
            id=1,
            dues_paid_overridden_on=datetime(2020, 1, 1),
            onboarded_on=datetime(2020, 1, 2),
            tags=[tag1, tag2]
        )
        m.email_address = SUPER_USER_EMAIL
        m.notes = "Some notes on this member"
        session.add_all([tag1, tag2, m])
        role1 = Role(
            member=m,
            role="member",
            date_created=datetime(2016, 1, 1).astimezone(pytz.utc))
        role2 = Role(member=m, role="admin", date_created=datetime(2016, 1, 1).astimezone(pytz.utc))
        session.add_all([role1, role2])
        membership = NationalMembershipData(
            member=m,
            ak_id="73750",
            first_name="Huey",
            last_name="Newton",
            address_line_1="123 Main St",
            city="Oakland",
            country="United States",
            zipcode="94612",
            dues_paid_until=datetime(3020, 6, 1, 0).astimezone(pytz.utc)
        )
        session.add(membership)

        phone_number = PhoneNumber(member_id=1, name="Home", number="425-123-2345")
        session.add(phone_number)

        meeting1 = Meeting(
            id=1,
            short_id=1,
            name="General Meeting 1",
            start_time=datetime(2017, 1, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 1, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )
        meeting2 = Meeting(
            id=2,
            short_id=2,
            name="General Meeting 2",
            start_time=datetime(2017, 2, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 2, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )
        meeting3 = Meeting(
            id=3,
            short_id=3,
            name="General Meeting 3",
            start_time=datetime(2017, 3, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 3, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )
        meeting4 = Meeting(
            id=4,
            short_id=None,
            name="General Meeting 4",
            start_time=datetime(2017, 4, 1, 0).astimezone(pytz.utc),
            end_time=datetime(2017, 4, 1, 1).astimezone(pytz.utc),
            owner_id=m.id,
        )

        attendee2 = Attendee(meeting=meeting2, member=m)
        attendee3 = Attendee(meeting=meeting3, member=m)

        committee = Committee(id=1, name="Testing Committee")
        # Even tho the member is on this committee,
        # it never is mentioned in their API responses because it's inactive
        inactive_committee = Committee(id=2, name="Inactive Committee", inactive=True)
        inactive_role = Role(
            member=m,
            role="member",
            date_created=datetime(2016, 1, 1).astimezone(pytz.utc),
            committee_id=2,
        )

        topic1 = InterestTopic(id=1, name="SocFem")
        topic2 = InterestTopic(id=2, name="Tech")

        interest1 = Interest(member_id=m.id, topic_id=topic1.id)

        session.add_all(
            [
                meeting1,
                attendee2,
                attendee3,
                meeting4,
                committee,
                inactive_committee,
                inactive_role,
                topic1,
                topic2,
                interest1,
            ]
        )

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_member_list_status_code(self):
        response = self.app.get("/member/list", content_type="application/json")
        result = response.status_code
        expected = 200

        assert result == expected

    def test_member_list_deprecated(self):
        response = self.app.get("/member/list", content_type="application/json")
        result = json.loads(response.data)
        expected = [
            {
                "id": 1,
                "name": "",
                "email": SUPER_USER_EMAIL,
                "eligibility": {
                    "is_eligible": True,
                    "message": "eligible (Feb, Mar)",
                    "dues_are_paid": True,
                    "meets_attendance_criteria": True,
                    "active_in_committee": False,
                    "num_votes": 1,
                },
            },
        ]

        assert result == expected

    def test_member_list(self):
        params = {
            "page_size": 1,
            "cursor": None,
        }
        response = self.app.get(
            "/member/list", content_type="application/json", query_string=params
        )
        result = json.loads(response.data)
        expected = {
            "members": [
                {
                    "id": 1,
                    "name": "",
                    "email": SUPER_USER_EMAIL,
                    "eligibility": {
                        "is_eligible": True,
                        "message": "eligible (Feb, Mar)",
                        "dues_are_paid": True,
                        "meets_attendance_criteria": True,
                        "active_in_committee": False,
                        "num_votes": 1,
                    },
                    "membership": {
                        "active": True,
                        "ak_id": "73750",
                        "do_not_call": False,
                        "first_name": "Huey",
                        "middle_name": None,
                        "last_name": "Newton",
                        "city": "Oakland",
                        "zipcode": "94612",
                        "join_date": None,
                        "phone_numbers": ["425-123-2345"],
                        "status": 'GOOD_STANDING',
                        "dues_paid_until": datetime(3020, 6, 1, 0).astimezone(pytz.utc).isoformat(),
                        "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                            pytz.utc
                        ).isoformat()
                    },
                }
            ],
            "has_more": False,
            "cursor": "1",
        }

        assert result == expected

    def do_search_test(
            self, params, should_match_user, expected_has_more, expected_cursor
    ):
        response = self.app.get(
            "/member/search", content_type="application/json", query_string=params
        )

        response_code = response.status_code
        assert response_code == 200

        result = json.loads(response.data)

        members = []

        if should_match_user:
            members += [
                {
                    "id": 1,
                    "name": "",
                    "email": SUPER_USER_EMAIL,
                    "eligibility": {
                        "is_eligible": True,
                        "message": "eligible (Feb, Mar)",
                        "dues_are_paid": True,
                        "meets_attendance_criteria": True,
                        "active_in_committee": False,
                        "num_votes": 1,
                    },
                    "membership": {
                        "active": True,
                        "ak_id": "73750",
                        "do_not_call": False,
                        "first_name": "Huey",
                        "middle_name": None,
                        "last_name": "Newton",
                        "city": "Oakland",
                        "zipcode": "94612",
                        "join_date": None,
                        "phone_numbers": ["425-123-2345"],
                        "status": 'GOOD_STANDING',
                        "dues_paid_until": datetime(3020, 6, 1, 0).astimezone(pytz.utc).isoformat(),
                        "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                            pytz.utc
                        ).isoformat()
                    },
                }
            ]
        expected = {
            "members": members,
            "has_more": expected_has_more,
            "cursor": expected_cursor,
        }

        assert result == expected

    def test_member_search_null_cursor(self):
        assert "@" in SUPER_USER_EMAIL
        query_str = SUPER_USER_EMAIL.split("@", 1)[
            0
        ]  # query for part of email before @
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": None,
        }

        self.do_search_test(
            params=params,
            should_match_user=True,
            expected_has_more=False,
            expected_cursor="1",
        )

    def test_member_search_nonnull_cursor(self):
        assert "@" in SUPER_USER_EMAIL
        query_str = SUPER_USER_EMAIL.split("@", 1)[
            0
        ]  # query for part of email before @
        params = {
            "query": query_str,
            "page_size": 1,
            "cursor": "1",
        }

        self.do_search_test(
            params=params,
            should_match_user=False,
            expected_has_more=False,
            expected_cursor="1",
        )

    def test_member(self):
        response = get_json(self.app, "/member")
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            "id": 1,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "phone_numbers": [{"phone_number": "425-123-2345", "name": "Home"}],
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
            ]
        }

    def test_member_details(self):
        response = get_json(self.app, "/member/details")
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            "id": 1,
            "do_not_call": False,
            "do_not_email": False,
            "is_eligible": True,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "phone_numbers": [{"phone_number": "425-123-2345", "name": "Home"}],
            },
            "notes": "Some notes on this member",
            "membership": {
                "address": ["123 Main St", "Oakland 94612"],
                "phone_numbers": ["425-123-2345"],
                "dues_paid_until": datetime(3020, 6, 1).astimezone(pytz.utc).isoformat(),
                "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(pytz.utc).isoformat(),
                "status": "GOOD_STANDING"
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat()
                },
            ],
            "meetings": [
                {"meeting_id": 2, "name": "General Meeting 2"},
                {"meeting_id": 3, "name": "General Meeting 3"},
            ],
            "votes": [],
            "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
            "suspended_on": None,
            "left_chapter_on": None,
            "dues_are_paid": True,
            "meets_attendance_criteria": True,
            "active_in_committee": False,
            'tags': [
                {
                    'date_added': '2020-10-01T00:00:00+00:00',
                    'id': 10,
                    'name': 'Tag 1'
                },
                {
                    'date_added': '2020-10-01T00:00:00+00:00',
                    'id': 11,
                    'name': 'Tag 2'
                }
            ]
        }

    def test_member_info(self):
        response = get_json(self.app, "/admin/member/details?member_id=1")
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {
            "id": 1,
            "do_not_call": False,
            "do_not_email": False,
            "is_eligible": True,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "phone_numbers": [{"phone_number": "425-123-2345", "name": "Home"}],
            },
            "notes": "Some notes on this member",
            "membership": {
                "address": ["123 Main St", "Oakland 94612"],
                "phone_numbers": ["425-123-2345"],
                "dues_paid_until": datetime(3020, 6, 1).astimezone(pytz.utc).isoformat(),
                "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(pytz.utc).isoformat(),
                "status": "GOOD_STANDING"
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
            ],
            "meetings": [
                {"meeting_id": 2, "name": "General Meeting 2"},
                {"meeting_id": 3, "name": "General Meeting 3"},
            ],
            "votes": [],
            "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
            "suspended_on": None,
            "left_chapter_on": None,
            "dues_are_paid": True,
            "meets_attendance_criteria": True,
            "active_in_committee": False,
            'tags': [
                {
                    'date_added': '2020-10-01T00:00:00+00:00',
                    'id': 10,
                    'name': 'Tag 1'
                },
                {
                    'date_added': '2020-10-01T00:00:00+00:00',
                    'id': 11,
                    'name': 'Tag 2'
                }
            ]
        }

    def test_add_member(self):
        first_name = "Eugene"
        last_name = "Debs"
        email_address = "debs.1855@gmail.com"
        payload = {
            "email_address": email_address,
            "first_name": first_name,
            "last_name": last_name,
        }
        response = post_json(self.app, "/member", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result["status"] == "success"
        assert result["data"] == {
            "account_created": False,
            "email_sent": False,
            "member": {
                "id": 2,
                "info": {
                    "first_name": first_name,
                    "last_name": last_name,
                    "email_address": email_address,
                    "phone_numbers": [],
                },
                "roles": [],
            },
            "role_created": False
        }

        session = Session()
        member = session.query(Member).filter_by(email_address=email_address).one()
        assert member.first_name == "Eugene"
        assert member.last_name == "Debs"
        assert member.email_address == "debs.1855@gmail.com"
        assert member.normalized_email == "debs1855@gmail.com"
        assert member.date_created is not None

    def test_update_member_dnc(self):
        payload = {"do_not_call": True, "do_not_email": True}
        response = put_json(self.app, "/member", payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result["status"] == "success"
        assert result["data"] == {
            "id": 1,
            "do_not_call": True,
            "do_not_email": True,
            "is_eligible": True,
            "info": {
                "first_name": None,
                "last_name": None,
                "email_address": SUPER_USER_EMAIL,
                "phone_numbers": [{"phone_number": "425-123-2345", "name": "Home"}],
            },
            "notes": "Some notes on this member",
            "membership": {
                "address": ["123 Main St", "Oakland 94612"],
                "phone_numbers": ["425-123-2345"],
                "dues_paid_until": datetime(3020, 6, 1).astimezone(pytz.utc).isoformat(),
                "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(pytz.utc).isoformat(),
                "status": "GOOD_STANDING"
            },
            "roles": [
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "admin",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
                {
                    "committee": "general",
                    "committee_name": "general",
                    "committee_id": -1,
                    "committee_inactive": False,
                    "role": "member",
                    "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                },
            ],
            "meetings": [
                {"meeting_id": 2, "name": "General Meeting 2"},
                {"meeting_id": 3, "name": "General Meeting 3"},
            ],
            "votes": [],
            "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
            "suspended_on": None,
            "left_chapter_on": None,
            "dues_are_paid": True,
            "meets_attendance_criteria": True,
            "active_in_committee": False,
            'tags': [
                {
                    'date_added': '2020-10-01T00:00:00+00:00',
                    'id': 10,
                    'name': 'Tag 1'
                },
                {
                    'date_added': '2020-10-01T00:00:00+00:00',
                    'id': 11,
                    'name': 'Tag 2'
                }
            ]
        }

    def test_add_update_member_non_json(self):
        response = self.app.post("/member", data=None)
        assert response.status_code == 400

        response = self.app.put("/member", data=None)
        assert response.status_code == 400

    def test_add_attendee(self):
        payload = {
            "meeting_id": 1,
            "member_id": 1,
        }
        response = post_json(self.app, "/member/attendee", payload=payload)
        assert response.status_code == 200

        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=1, member_id=1).count() == 1

    def test_add_attendee_duplicate(self):
        session = Session()
        assert session.query(Attendee).filter_by(meeting_id=3, member_id=1).count() == 1

        payload = {
            "meeting_id": 3,
            "member_id": 1,
        }
        response = post_json(self.app, "/member/attendee", payload=payload)
        assert response.status_code == 200

        assert session.query(Attendee).filter_by(meeting_id=3, member_id=1).count() == 1

    def test_add_attendee_non_json(self):
        response = self.app.post("/member/attendee", data=None)
        assert response.status_code == 400

    def test_make_admin(self):
        payload = {"email_address": SUPER_USER_EMAIL, "committee": 1}
        response = post_json(self.app, "/admin", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

        session = Session()
        assert (
                session.query(Role)
                .filter_by(member_id=1, committee_id=1, role="admin")
                .count()
                == 1
        )

    def test_make_admin_non_json(self):
        response = self.app.post("/admin", data=None)
        assert response.status_code == 400

    def test_add_committee_member_role(self):
        payload = {
            "member_id": 1,
            "committee_id": 1,
            "role": "member",
        }
        response = post_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

    def test_add_committee_admin_role(self):
        session = Session()
        committee_member_role = Role(member_id=1, committee_id=1, role="member")
        committee_admin_role = Role(member_id=1, committee_id=1, role="admin")
        session.add(committee_member_role)
        session.commit()
        payload = {
            "member_id": committee_admin_role.member_id,
            "committee_id": committee_admin_role.committee_id,
            "role": committee_admin_role.role,
        }
        response = post_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

    def test_add_chapter_member_role_duplicate(self):
        payload = {
            "member_id": 1,
            "committee_id": None,
            "role": "admin",
        }
        response = post_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 409

    def test_add_member_role_non_json(self):
        response = self.app.post("/admin", data=None)
        assert response.status_code == 400

    def test_remove_member_role(self):
        payload = {
            "member_id": 1,
            "committee_id": None,
            "role": "admin",
        }
        response = delete_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 200

        result = json.loads(response.data)
        assert result == {"status": "success"}

    def test_remove_member_role_nonexistent(self):
        payload = {
            "member_id": 1,
            "committee_id": 1,
            "role": "admin",
        }
        response = delete_json(self.app, "/member/role", payload=payload)
        assert response.status_code == 404

    def test_remove_member_role_non_json(self):
        response = self.app.delete("/member/role", data=None)
        assert response.status_code == 400

    def test_get_interests(self):
        response = self.app.get("/member/interests", data=None)
        assert response.status_code == 200
        assert json.loads(response.data) == {"topics": ["SocFem"]}

    def test_update_interests_unknown_topic(self):
        payload = {"topics": ["Market Urbanism"]}
        response = put_json(self.app, "/member/interests", payload)
        assert response.status_code == 400

    def test_update_interests_bad_request(self):
        payload = {"topics": "Tech"}
        response = put_json(self.app, "/member/interests", payload)
        assert response.status_code == 400

    def test_update_interests_good_request(self):
        payload = {"topics": ["SocFem", "Tech"]}
        response = put_json(self.app, "/member/interests", payload)
        assert response.status_code == 200
        response_body = json.loads(response.data)
        diff = DeepDiff(response_body, payload, ignore_order=True).to_dict()
        assert diff == {}

    def test_get_notes(self):
        response = get_json(self.app, "/admin/member/details?member_id=1")
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert json_response["notes"] == "Some notes on this member"

    def test_set_notes(self):
        """Set and re-read notes to verify integrity"""
        payload = {"notes": "These are some notes for the member"}
        response = put_json(self.app, "/admin/member/notes?member_id=1", payload)
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert json_response["notes"] == "These are some notes for the member"

    def test_set_notes_bad_response(self):
        payload = "These are some notes for the member"
        response = put_json(self.app, "/admin/member/notes?member_id=1", payload)
        assert response.status_code == 400

    def test_update_member_details(self):
        payload = {
            "first_name": "Huey",
            "last_name": "Newton",
            "pronouns": "he/him;they/them",
        }
        response = patch_json(self.app, "/admin/member?member_id=1", payload)
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert json_response == {
            "data": {
                "id": 1,
                "do_not_call": False,
                "do_not_email": False,
                "is_eligible": True,
                "info": {
                    "first_name": "Huey",
                    "last_name": "Newton",
                    "pronouns": "he/him;they/them",
                    "email_address": SUPER_USER_EMAIL,
                    "phone_numbers": [{"phone_number": "425-123-2345", "name": "Home"}],
                },
                "notes": "Some notes on this member",
                "membership": {
                    "address": ["123 Main St", "Oakland 94612"],
                    "phone_numbers": ["425-123-2345"],
                    "dues_paid_until": datetime(3020, 6, 1).astimezone(pytz.utc).isoformat(),
                    "dues_paid_overridden_on": datetime(2020, 1, 1).astimezone(
                        pytz.utc
                    ).isoformat(),
                    "status": "GOOD_STANDING"
                },
                "roles": [
                    {
                        "committee": "general",
                        "committee_name": "general",
                        "committee_id": -1,
                        "committee_inactive": False,
                        "role": "admin",
                        "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                    },
                    {
                        "committee": "general",
                        "committee_name": "general",
                        "committee_id": -1,
                        "committee_inactive": False,
                        "role": "member",
                        "date_created": datetime(2016, 1, 1).astimezone(pytz.utc).isoformat(),
                    },
                ],
                "meetings": [
                    {"meeting_id": 2, "name": "General Meeting 2"},
                    {"meeting_id": 3, "name": "General Meeting 3"},
                ],
                "votes": [],
                "onboarded_on": datetime(2020, 1, 2).astimezone(pytz.utc).isoformat(),
                "suspended_on": None,
                "left_chapter_on": None,
                "dues_are_paid": True,
                "meets_attendance_criteria": True,
                "active_in_committee": False,
                'tags': [
                    {
                        'date_added': '2020-10-01T00:00:00+00:00',
                        'id': 10,
                        'name': 'Tag 1'
                    },
                    {
                        'date_added': '2020-10-01T00:00:00+00:00',
                        'id': 11,
                        'name': 'Tag 2'
                    }
                ]
            },
            "status": "success"
        }

    def test_update_member_details_unknown_field(self):
        payload = {"horoscope": "aquarius"}
        response = patch_json(self.app, "/admin/member?member_id=1", payload)
        assert response.status_code == 400

    def test_add_phone_number(self):
        payload = {"phone_number": "408-224-5555", "name": "Cell"}
        response = post_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 2
        assert json_response == [
            {"phone_number": "408-224-5555", "name": "Cell"},
            {"phone_number": "425-123-2345", "name": "Home"},
        ]

    def test_add_phone_number_invalid_request(self):
        payload = "425-123-2345"
        response = post_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 400

    def test_add_phone_number_conflict(self):
        payload = {"phone_number": "425-123-2345", "name": "Other home"}
        response = post_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 409

    def test_delete_phone_number(self):
        payload = {"phone_number": "425-123-2345"}
        response = delete_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 0

    def test_delete_phone_number_non_existent(self):
        payload = {"phone_number": "808-808-8080"}
        response = delete_json(
            self.app, "/admin/member/phone_numbers?member_id=1", payload
        )
        assert response.status_code == 404

    def test_add_email_address(self):
        payload = {
            "email_address": "hello@world.com",
        }
        response = post_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 1
        assert json_response == [
            {
                "email_address": "hello@world.com",
                "name": None,
                "preferred": False,
                "verified": False,
            }
        ]

    def test_delete_email_address(self):
        payload = {
            "email_address": "hello@world.com",
        }
        response = post_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 1
        assert json_response == [
            {
                "email_address": "hello@world.com",
                "name": None,
                "preferred": False,
                "verified": False,
            }
        ]
        response = delete_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 200
        json_response = response.get_json(force=True, silent=True)
        assert json_response is not None
        assert len(json_response) == 0

    def test_delete_email_address_not_found(self):
        payload = {
            "email_address": "hello@world.com",
        }
        response = delete_json(
            self.app, "/admin/member/email_addresses?member_id=1", payload
        )
        assert response.status_code == 404

    def test_add_tag_should_add_tag_and_member_association(self):
        response = put_json(self.app, "/member/1/tags", {
            'tags': ['test1', 'test2']
        })
        assert response.status_code == 200

        session = Session()

        tags = session.query(Tag).all()
        assert len(tags) > 0

        tag_names = [tag.name for tag in tags]
        assert 'test1' in tag_names
        assert 'test2' in tag_names

        member = session.query(Member).get(1)
        assert member is not None

        tag_names = [tag.name for tag in member.tags]
        assert 'test1' in tag_names
        assert 'test2' in tag_names

    def test_remove_tag_should_remove_member_tags(self):
        response = delete_json(self.app, "/member/1/tags", {
            'tags': ['Tag 1', 'Tag 2']
        })
        assert response.status_code == 200

        session = Session()

        member = session.query(Member).get(1)
        assert member is not None

        tag_ids = [tag.id for tag in member.tags]
        assert 10 not in tag_ids
        assert 11 not in tag_ids

    def test_import_members(self):
        with open("tests/membership/web/members.csv", "rb") as import_file:
            payload = {
                "file": (BytesIO(import_file.read()), "members.csv"),
                "next_reg_meeting": "2020-10-01"
            }
            response = self.app.put(
                "/import", data=payload, content_type="multipart/form-data"
            )
            assert response.status_code == 200
