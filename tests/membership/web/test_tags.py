import json
from datetime import datetime

import pytz
from freezegun import freeze_time
from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse

from config import SUPER_USER_EMAIL
from membership.database.base import metadata, engine, Session
from membership.database.models import Member, Role, PhoneNumber, Tag
from membership.web.base_app import app


class TestTag:

    member_id = 4927

    def setup(self):
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        session = Session()

        tag1 = Tag(id=10, name="Tag 1", date_added=datetime(2020, 10, 1).astimezone(pytz.utc))
        tag2 = Tag(id=11, name="Tag 2", date_added=datetime(2020, 10, 1).astimezone(pytz.utc))

        m = Member(
            id=self.member_id,
            email_address=SUPER_USER_EMAIL,
            phone_numbers=[
                PhoneNumber(member_id=self.member_id, name="Home", number="425-123-2345")
            ],
            all_roles=[
                Role(
                    member_id=self.member_id,
                    role="member",
                    date_created=datetime(2016, 1, 1).astimezone(pytz.utc)),
                Role(
                    member_id=self.member_id,
                    role="admin",
                    date_created=datetime(2016, 1, 1).astimezone(pytz.utc))
            ],
            tags=[tag1, tag2]
        )

        session.add_all([tag1, tag2, m])

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    @freeze_time("2020-10-01")
    def test_get_tags_should_return_all_tags(self):
        response = self.app.get("/tags", content_type="application/json")
        status = response.status_code
        payload = json.loads(response.data)

        assert status == 200
        assert payload == [
            {
                'id': 10,
                'name': 'Tag 1',
                'date_added': datetime(2020, 10, 1).astimezone(pytz.utc).isoformat()
            },
            {
                'id': 11,
                'name': 'Tag 2',
                'date_added': datetime(2020, 10, 1).astimezone(pytz.utc).isoformat()
            }
        ]

    @freeze_time("2020-10-01")
    def test_delete_tag_should_remove_tag_and_member_associations(self):
        response = self.app.delete("/tags/10", content_type="application/json")
        status = response.status_code

        assert status == 200

        session = Session()

        tags = session.query(Tag).all()
        member = session.query(Member).get(self.member_id)

        assert len(tags) == 1
        assert member is not None
        assert len(member.tags) == 1

    @freeze_time("2020-10-01")
    def test_get_members_with_tag_should_return_members_with_tag(self):
        response = self.app.get("/tags/10/members", content_type="application/json")
        status = response.status_code
        payload = json.loads(response.data)

        assert status == 200
        assert len(payload) == 1
        assert payload[0]['id'] == self.member_id
